# CHANGELOG

## V2.8.8

- Added new Layout
- Some bug fixes
- Updated to Bootstrap 4.2.1

## V2.8.4

- Added Language translation support in Datepicker
- Some bug fixes

## V2.8.3

- Calendar page redesign
- Some bug fixes

## V2.8.1

- Fixed Ace-editor cursor positioning bug
- Fixed some visiblity issues in dark-mode
- Added language translation in full calendar
- Fixed content overflow issue in wizard

## V2.8.0

- Added Dark Theme
- Some bug fixes

## V2.7.2

- Added Lazy-Load feature

## V2.7.1

- New Horizontal layout
- Updated to Bootstrap 4.1.3
- Improved gulp tasks
- Some bug fixes
- Added new widget cards
- Design improvements

## V2.0.0

- Updated to Bootstrap 4.1.1
- Concatenated template CSS and JS files
- Added some Gulp tasks
- Design improvements
- Minor bug fixes
- Improved dashboard design and added one new dashboard
- Redesigned horizontal layouts
- Redesigned calendar page
- Brand new profile page
- Redesigned login and register page

## V1.0.0

- Initial release
