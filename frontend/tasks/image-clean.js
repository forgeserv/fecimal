'use strict';

const gulp = require('gulp'),
      del  = require('del');

module.exports = options => {

  return () => {
    return del([`${options.src}/images/`]);
  };

};
