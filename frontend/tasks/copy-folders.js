'use strict';

const gulp = require('gulp');

module.exports = options => {

  return () => {
    return gulp.src(options.foldersToCopy, { dot: true })
               .pipe(gulp.dest(`./${options.dest}`));
  };

};
