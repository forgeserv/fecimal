'use strict';

const gulp  = require('gulp'),
      fs    = require('fs');

module.exports = options => {

  // If index.html exist - open it, else show folder
  let listDirectory = fs.existsSync(`${options.pages}/${options.mainHtml}`) ? false : true;

	return () => {
		options.browserSync.init({
			notify: false,
      server: {
        directory: listDirectory,
        baseDir: [options.pages, options.build],
        routes: {
          '/assets': options.build
        }
      },
			snippetOptions: {
        rule: {
          match: /$/i,
          fn: (snippet, match) => snippet + match
        }
			},
			port: 3000
		});
	};

};
