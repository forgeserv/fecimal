module.exports = {
  dir: {
    tasks: 'tasks',
    src: 'src',
    pages: 'pages',
    build: 'assets',
    prod: '../public/assets'
  },
  file: {
    mainHtml: 'index.html',
    mainJs: 'app.js',
    vendorJs: 'vendor.js',
    vendorJsMin: 'vendor.min.js',
    mainScss: 'style.scss',
    mainScssMin: 'style.min.css'
  },
  task: {
    htmlHint: 'html-hint',
    jsHint: 'js-hint',
    buildCustomJs: 'build-custom-js',
    buildJsVendors: 'build-js-vendors',
    buildSass: 'build-sass',
    buildSassFiles: 'compile-sass-files',
    buildSassProd: 'build-sass-production',
    imageMin: 'image-min',
    imageClean: 'image-clean',
    cleanProd: 'clean-production',
    cleanBuild: 'clean-build',
    copyFolders: 'copy-folders',
    copyFoldersProduction: 'copy-folders-production',
    browserSync: 'browser-sync-server',
    watch: 'watch',
  },
  autoprefixer: {
    versions: 'last 4 versions'
  },
  imageExtensions: 'jpg|jpeg|png|svg|gif|ico|tiff',
  getPathesForSassCompiling: function() {
    return {
      files: [],
      isGcmq: false
    };
  },
  getPathesToCopyForProduction: function() {
    return [
      `./${this.dir.build}/**/*`,
      `./${this.dir.pages}/*.html`
    ];
  },
  getPathesToCopy: function() {
    return [
      `./${this.dir.src}/**`,
      `!{${this.dir.src}/images,${this.dir.src}/images/**}`,
      `!{${this.dir.src}/js,${this.dir.src}/js/**}`,
      `!{${this.dir.src}/scss,${this.dir.src}/scss/**}`,
      `!{${this.dir.src}/vendor,${this.dir.src}/vendor/**}`,
      // `!{${this.dir.src}/placeholder,${this.dir.src}/placeholder/**}`,
      `!{${this.dir.src}/plugins,${this.dir.src}/plugins/**}`,
      `!{${this.dir.pages},${this.dir.pages}/**}`,
    ];
  }
};
