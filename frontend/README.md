<img src="https://raw.githubusercontent.com/brz-digital/brz-starter/develop/src/images/brz.png" width="80" height="60" />

> # BRZ Starter
> 
> BRZ Starter is a boilerplate template for building websites.

### Setup

```$ yarn```

### Running

```$ yarn start```

### Build

```$ yarn build```

### How to installing frontend dependencies?

We use yarn to manage our frontend dependencies. For example, if you want to install jQuery, you can run `yarn add jquery`.

### For default, contains

- jQuery
- Bulma
- Include Media
- Swiper
- Jquery Mask
