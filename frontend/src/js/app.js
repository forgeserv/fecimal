/**
 * App
 */

import Common from './Common';
import Slide from './modules/Slide';
import GoogleMap from './modules/GoogleMap';
import Player from './modules/Player';
import Collapsible  from './modules/Collapsible';
import Gallery  from './modules/Gallery';

const csrfToken = $('meta[name="csrf-token"]').attr('content') || null;

const availableModules = {
  Common,
  Slide,
  GoogleMap,
  Player,
  Collapsible,
  Gallery
};

window.modules = {};

(($) => {
  'use strict';

  $(() => {
    const htmlModules = $('[data-module]');

    // Loading htmlModules if they are in availableModules
    htmlModules.each((key, value) => {
      const module = $(value).data('module');

      if (Object.prototype.hasOwnProperty.call(availableModules, module)) {
        window.modules[module] = new availableModules[module]();
      } else {
        console.log(`The module "${module}" does not exists.`);
      }
    });
  });
})(jQuery);

document.addEventListener('DOMContentLoaded', function(event) {
  console.log("%cMade in BRZ Digital! \ud83d\ude42", "font-size: 16px; color: #a7bf34");
});
