/**
 * Common Module
 */

import Share from './modules/Share';
import Send from './modules/Send';
import Menu from './modules/Menu';
import Slide from './modules/Slide';
import Accessibility from './modules/Accessibility';
import Header from './modules/Header';

class Common {
  constructor() {
    // Call modules
    new Menu();
    new Slide();
    new Send();
    new Share();
    // new Accessibility();
    new Header();

    // Call methods
    this.fixedI10();
    this.disableZoomGesture();
    this.scrollToTarget();

  }

  fixedI10() {
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
      const msViewportStyle = document.createElement('style');
      msViewportStyle.appendChild(document.createTextNode('@-ms-viewport{width:auto!important}'));
      document.querySelector('head').appendChild(msViewportStyle);
    }
  }

  scrollToTarget() {
    $('.js-scroll-to').on('click', function(e) {
      e.preventDefault();

      let target = $(this).attr('href');

      $('html, body').animate({
        scrollTop: $(target).offset().top - 50
      }, 800);
    });
  }


  disableZoomGesture() {
    document.addEventListener('gesturestart', (e) => {
      e.preventDefault();
    });
  }
}

export default Common;
