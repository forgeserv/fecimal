class Collapsible {
  constructor() {
    console.log('>>> Collapsible constructor');

    $('#collapse').collapsible({
      accordion: true,
      arrowRclass: 'icon-down-before',
      arrowDclass: 'icon-up-before',
      animate: true
    });
  }
}

export default Collapsible;
