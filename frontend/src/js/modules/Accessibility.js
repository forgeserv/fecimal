class Accessibility {
    constructor() {
        console.log('>>> Accessibility constructor');

        // Units
        this.units = ['px', 'rem', 'cm', 'em', 'ex', 'in', 'mm', 'pc', 'pt', 'vh', 'vw', 'vmin'];

        // Storage key
        this.accessibility_storage = 'accessibility-config';

        // Default options
        this.options = {
            grayscale: false,
            maxZoomLevel: 1.2,
            minZoomLevel: 0.8,
            zoomStep: 0.1,
            zoom: 1,
            textSelector: 'h2,h3,h4,h5,h6,p,a,span,small',
            invert: false
        };

        // Init
        this.init();
    }

    init() {
        const that = this;
        const defaultOptions = this.options;
        const userOptions = that.getUserOptions();
        const options = $.extend( defaultOptions, userOptions );

        let html = $('html'),
            body = $('body'),
            zoomOutButton = $('.js-accessibility-zoom-out'),
            zoomInButton = $('.js-accessibility-zoom-in'),
            grayscaleButton = $('.js-accessibility-grayscale');

        // Add class to transition zoom
        html.addClass('accessibility-zoom');

        // Zoom in button
        zoomInButton.click(function (e) {
            e.preventDefault();
            options.zoom = Math.min(options.maxZoomLevel, options.zoom + options.zoomStep);
            that.apply();
        });

        // Zoom out buttom
        zoomOutButton.click(function (e) {
            e.preventDefault();
            options.zoom = Math.max(options.minZoomLevel, options.zoom - options.zoomStep);
            that.apply();
        });

        // Grayscale button
        grayscaleButton.click(function (e) {
            e.preventDefault();

            if (options.grayscale == false) {
                options.grayscale = true;
            } else {
                options.grayscale = false;
            }

            that.apply();
        });

        // Apply text zoom options
        that.applyTextZoom(options.textSelector, 1);

        // Apply options
        that.apply();

        // Init shortcuts
        that.shortcuts();
    }

    getUnit(fontSize) {
        fontSize = fontSize || '';
        return this.units.filter(function (unit) {
            return fontSize.match(new RegExp(unit + '$', 'gi'));
        }).pop();
    }

    getUserOptions() {
        let data;
        const accessibility_storage = this.accessibility_storage;

        try {
            data = localStorage.getItem(accessibility_storage);
            data = JSON.parse(data);
        } catch (e) {}

        if (data && (typeof data === 'undefined' ? 'undefined' : typeof(data)) === 'object') {
            return data;
        } else {
            return {};
        }
    }

    setUserOptions(options) {
        localStorage.setItem(
            this.accessibility_storage,
            JSON.stringify(options)
        );
    }

    applyTextZoom(selector, zoom) {
        const that = this;
        $(selector).each(function () {
            let element = $(this);

            let originalFontSize = element.attr('data-accessibility-text-original');
            if (!originalFontSize) {
                originalFontSize = element.css('font-size');
                element.attr('data-accessibility-text-original', originalFontSize);
            }

            let units = that.getUnit(originalFontSize) || '';
            let fontSize = parseFloat(originalFontSize) * zoom;

            element.css('font-size', fontSize + units);
        });
    }

    apply() {
        const that = this;
        const options = this.options;

        let html = $('html');

        if(options.grayscale == false) {
            html.removeClass('grayscale');
        } else {
            html.addClass('grayscale');
        }

        // Zoom
        that.applyTextZoom(options.textSelector, options.zoom);

        // Set user options
        that.setUserOptions(options);
    }

    shortcuts() {

        const that = this;
        const defaultOptions = this.options;
        const userOptions = that.getUserOptions();
        const options = $.extend( defaultOptions, userOptions );

        shortcut.add('Ctrl+Shift+c', function() {
            options.grayscale += 100;

            if (options.grayscale > 100) {
                options.grayscale = 0;
            }

            that.apply();
        });

        shortcut.add('Ctrl+Shift+z', function() {
            options.zoom = Math.max(options.minZoomLevel, options.zoom - options.zoomStep);
            that.apply();
        });

        shortcut.add('Ctrl+Shift+x', function() {
            options.zoom = Math.min(options.maxZoomLevel, options.zoom + options.zoomStep);
            that.apply();
        });

        shortcut.add('Ctrl+Shift+1', function() {
            window.location.hash='main';
        });

        shortcut.add('Ctrl+Shift+2', function() {
            window.location='/';
        });

        shortcut.add('Ctrl+Shift+3', function() {
            window.location='/graduacao';
        });

        shortcut.add('Ctrl+Shift+4', function() {
            window.location='/pos-graduacao';
        });

        shortcut.add('Ctrl+Shift+5', function() {
            window.location='/novidades';
        });

        shortcut.add('Ctrl+Shift+6', function() {
            window.location='/contato';
        });
    }
}

export default Accessibility;