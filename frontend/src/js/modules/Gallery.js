class Gallery {
  constructor() {
    console.log('>>> Gallery constructor');

    $('.js-gallery').lightGallery({
      selector: '.card.-project .c-link',
      thumbnail: true,
      download: false,
      zoom: false,
      share: false
    });
  }
}

export default Gallery;
