class Header {

	constructor () {
		const that = this;

		const header = document.getElementById("js-header");

		let offset = 2 || header.offsetTop;

		window.onscroll = function() {

			that.sticky(header, offset);

		};

	}

	sticky(header, offset) {

	  if (window.pageYOffset > offset) {

	    header.classList.add("-sticky");

	  } else {

	    header.classList.remove("-sticky");

	  }

	}
}

export default Header;