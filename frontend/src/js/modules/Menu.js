class Menu {
  constructor() {

	const that = this;

    this.toggle('.js-toggle');

    this.submenu('.-has-submenu');

    $(window).resize(() => {
    	this.submenu('.-has-submenu');
    });

  }

  toggle(toggleClass) {
	    const that = this;

	    $(toggleClass).on('click', (e) => {

	      e.preventDefault();

	      $("body, .js-menu, .js-toggle, .circle").toggleClass('-open');

	    });
  }
  
  submenu(target) {
	const that = this;

  	if (!that.isMobile()) {

  		$(target)

	  		.mouseover( function(e) {

	  			$(this).addClass('open-submenu');

	  		})

	  		.mouseleave( function(e) {

				$(this).removeClass('open-submenu');

	  		});

	  	} else {

	  		$('.-has-submenu > .link').click( function(e) {
	  			e.preventDefault();

		  		$('.-has-submenu').toggleClass('open-submenu');

		  	});
	  	}
  	
  }

  isMobile(){
  	return $(window).width() < 767 ? true : false;
  }


}
export default Menu;
