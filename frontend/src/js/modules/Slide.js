class Slide {
  constructor() {
    console.log('>>> Slide constructor');

    const that = this;

    this.swiperInstances = {};

    $('.js-swiper').each((index, element) => {
      const $container = $(element);

      // Set swiper settings
      const settings = {
        onlyMobile:   $container.data('onlyMobile')   || false,
        mobileBreak:  $container.data('mobileBreak')  || 991,
        autoplay:     $container.data('autoplay')     || false,
        loop:         $container.data('loop')         || false,
        autoHeight:   $container.data('autoheight')   || false,
        perView:      $container.data('perView')      || 1,
        perViewXL:    $container.data('perViewXl')    || 1,
        perViewLG:    $container.data('perViewLg')    || 1,
        perViewMD:    $container.data('perViewMd')    || 1,
        perViewSM:    $container.data('perViewSm')    || 1,
        perViewXS:    $container.data('perViewXs')    || 1,

        perColumn:      $container.data('perColumn')      || 1,
        perColumnXL:    $container.data('perColumnXl')    || 1,
        perColumnLG:    $container.data('perColumnLg')    || 1,
        perColumnMD:    $container.data('perColumnMd')    || 1,
        perColumnSM:    $container.data('perColumnSm')    || 1,
        perColumnXS:    $container.data('perColumnXs')    || 1,

        navigationTarget:   $container.data('navigationTarget') || false,
        paginationTarget:   $container.data('paginationTarget') || false,

        spaceBetween: $container.data('spaceBetween') || 0,
        effect:       $container.data('effect')       || 'fade',
        pagination:   $container.data('pagination')   || 'bullets',
        direction:    $container.data('direction')    || 'horizontal',
      };

      // Add class index
      $container.addClass(`swiper-${index}`);
      $container.find('.slide-counter').addClass(`pagination-counter-${index}`);
      $container.find('.slide-pagination').addClass(`pagination-${index}`);

      // If onlymobile is true
      if (settings.onlyMobile === true) {
        // Add class swiper-only-mobile
        $container.addClass('swiper-only-mobile');

        // Instance swiper if width <= settings.mobileBreakpoint
        if ($(window).outerWidth() <= settings.mobileBreak) {
          that.startSwiper($container, index, settings);
        }

        // Check swiper on resize, if <= settings.mobileBreakpoint, instance
        that.startSwiperOnResize($container, index, settings);
      } else {
        // Instance swiper
        that.startSwiper($container, index, settings);
      }
       that.newNavigation(settings);
        that.newPagination(settings);
    });
  }

  newNavigation(set) {
    const that = this;
    let target = set.navigationTarget;

    $.each(that.swiperInstances, function (index, element){

      console.log(element);

      if(target) {
        $(target).find('.link').on('click', function(e){

          e.preventDefault();

          let index =  $(this).data('slide');

          $(target).find('.link').removeClass('-active');

          $(this).addClass('-active');

          element.slideTo(index, 1000);
        });
      }
    });
  }

  newPagination(set) {
    const that = this;
    let target = set.paginationTarget;

    $.each(that.swiperInstances, function (index, element){
      if(target) {
        $(target).find('.swiper-button-prev').on('click', function(e){
          e.preventDefault();
          element.slidePrev();
        });
        $(target).find('.swiper-button-next').on('click', function(e){
          e.preventDefault();
          element.slideNext();
        });
      }
    });
  }

  slideTo(id, i) {
    const that = this;
    $.each(that.swiperInstances, function (index, element){
        if(element.params.pagination-target) {
            setTimeout(function () {
                element.update();
                element.slideTo(i, 0);
            }, 50);
        }else {
            return "false";
        }
    });
  }

  startSwiper(container, index, settings) {
    const that = this;

    container.addClass('swiper');
    container.find('.wrapper').addClass('swiper-wrapper');
    container.find('.slide-item').addClass('swiper-slide');

    that.swiperInstances[index] = new Swiper(`.swiper-${index}`, {
      pagination: {
        el: `.pagination-counter-${index}`,
        clickable: true,
        type: settings.pagination,
      },
      navigation: {
        nextEl: `.pagination-${index} .swiper-button-next`,
        prevEl: `.pagination-${index} .swiper-button-prev`,
      },
      autoHeight: settings.autoHeight,
      direction: settings.direction,
      loop: settings.loop,
      autoplay: settings.autoplay,
      slideClass: 'swiper-slide',
      wrapperClass: 'swiper-wrapper',
      slidesPerView: settings.perView,
      spaceBetween: settings.spaceBetween,
      effect: settings.effect,
      simulateTouch: false,
      breakpoints: {
        2600: {
          slidesPerView: settings.perViewXL,
          slidesPerColumn: settings.perColumnXL,
        },
        1599: {
          slidesPerView: settings.perViewLG,
          slidesPerColumn: settings.perColumnLG,
        },
        1199: {
          slidesPerView: settings.perViewMD,
          slidesPerColumn: settings.perColumnMD,
        },
        991: {
          slidesPerView: settings.perViewSM,
          slidesPerColumn: settings.perColumnSM,
        },
        767: {
          slidesPerView: settings.perViewXS,
          slidesPerColumn: settings.perColumnXS,
        },
      },
    });
  }

  startSwiperOnResize(container, index, settings) {
    const that = this;

    $(window).resize(() => {
      if ($(window).outerWidth() <= settings.mobileBreak && !that.swiperInstances[index]) {
        that.startSwiper(container, index, settings);
      } else if ($(window).outerWidth() >= settings.mobileBreak + 1) {
        container.removeClass('swiper');
        container.find('.wrapper').removeClass('swiper-wrapper');
        container.find('.slide-item').removeClass('swiper-slide');

        if (that.swiperInstances[index]) {
          that.swiperInstances[index].destroy(false, true);
          that.swiperInstances[index] = undefined;
        }
      }
    });
  }
}

export default Slide;
