module.exports = [
  './node_modules/jquery/dist/jquery.js',
  './node_modules/jquery-mask-plugin/dist/jquery.mask.js',
  './node_modules/swiper/dist/js/swiper.js',
  './node_modules/animejs/anime.min.js',
  './src/plugins/jquery.maps.js',
  './node_modules/collapsible.js/jquery.collapsible.js',
  './node_modules/lightgallery/dist/js/lightgallery-all.js',
  './node_modules/plyr/dist/plyr.min.js',
  './src/plugins/shortcut.js'
];
