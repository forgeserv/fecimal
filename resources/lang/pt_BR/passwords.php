<?php
return [
  'password' => 'A senha deve possuir no mínimo 8 caracteres e ser igual a confirmação.',
  'reset'    => 'Parabéns, a sua senha foi redefinida!',
  'sent'     => 'O link para redefinição de senha foi enviado para o seu e-mail.',
  'token'    => 'O token para recuperação de senha é inválido.',
  'user'     => 'Não encontramos nenhum usuário com esse e-mail!',
];
