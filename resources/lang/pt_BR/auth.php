<?php
return [
  'failed'   => 'Usuário ou senha inválidos.',
  'throttle' => 'Muitas tentativas de login, tente novamente em :seconds segundos.',
];
