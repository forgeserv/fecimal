@extends('admin.layouts.default')

@section('content')
  <div class="content-wrapper">
    @include('flash::message')
    {!! Form::model($result, ['route' => ['admin.projects_galleries.update', 'project_id' => $project->id, 'id' => $result->id], 'method' => 'PUT', 'novalidate', 'role' => 'form', 'files' => true]) !!}
      <div class="row">
        {{ Breadcrumbs::render('projects_galleries.edit', $project, $result) }}
        <div class="col-md-12 d-flex align-items-stretch grid-margin">
          <div class="row flex-grow">
            <div class="col-12">
              <div class="card">
                <div class="card-body reset-padding margin-1">
                  <div class="card-action-title">
                    <h3> Editar Galeria
                      <small class="text-muted"> Preencha os campos abaixo. </small>
                    </h3>
                  </div>
                  <div class="card-actions">
                    <div class="margin-btn">
                      <a href="{{ route('admin.projects_galleries.index', ['project_id' => $project->id]) }}" class="btn btn-danger btn-lg" title="Voltar"><i class="fa fa-angle-left"></i>Voltar</a>
                    </div>
                    <div class="">
                      <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-save"></i>Salvar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
       <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
          <div class="card review-card">
            <div class="card-header header-sm d-flex justify-content-between align-items-center sac">
              <h4 class="card-title"> <i class="icon-sm link-icon mdi mdi-pencil"></i> Detalhes</h4>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="form-group col-md-6 col-lg-6 col-sm-12">
                  {!! Form::label('name', 'Nome', []) !!}
                  {!! Form::text('name', old('name'), ['class' => 'form-control form-control-lg']) !!}
                  @if($errors->has('name'))
                    <span class="help-block">{{ $errors->first('name') }}</span>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    {!! Form::close() !!}
  </div>
@endsection
