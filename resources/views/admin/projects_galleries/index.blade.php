@extends('admin.layouts.default')

@section('content')
  <div class="content-wrapper">
    @include('flash::message')
    <div class="row">
      {{ Breadcrumbs::render('projects_galleries.index', $project) }}
    </div>
    <div class="row">
      <div class="col-12 grid-margin">
        <div class="card card-statistics">
          <div class="row">
            @can('view_projects')
            <div class="card-col col-xl-6 col-lg-6 col-md-6 col-6">
              <div class="card-body">
                <a href="{{ route('admin.projects.index') }}" class="hover-me" title="Voltar"></a>
                <div class="d-flex align-items-center justify-content-center flex-column flex-sm-row">
                  <i class="mdi mdi-arrow-left text-danger mr-0 mr-sm-4 icon-lg"></i>
                  <div class="wrapper text-center text-sm-left">
                    <p class="card-text mb-0">Voltar</p>
                    <div class="fluid-container">
                      <h3 class="mb-0 font-weight-medium">Projetos</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endcan
            @can('add_releases')
            <div class="card-col col-xl-6 col-lg-6 col-md-6 col-6">
              <div class="card-body">
                <a href="{{ route('admin.projects_galleries.create', ['project_id' => $project->id]) }}" class="hover-me" title="Adicionar"></a>
                <div class="d-flex align-items-center justify-content-center flex-column flex-sm-row">
                  <i class="mdi mdi-account-plus text-success mr-0 mr-sm-4 icon-lg"></i>
                  <div class="wrapper text-center text-sm-left">
                    <p class="card-text mb-0">Adicionar</p>
                    <div class="fluid-container">
                      <h3 class="mb-0 font-weight-medium">Galeria</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endcan
          </div>
        </div>
      </div>
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Galeria - {{ $project->title }}</h4>
            <div class="row">
              @foreach($results as $result)
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                <div class="card review-card m-bottom">
                  <div class="card-header header-sm d-flex justify-content-between align-items-center">
                    <h4 class="card-title">{{ $result->name }}</h4>
                    <div class="cards-actions">

                      @can('edit_projects_submodules')
                      <a href="{{ route('admin.projects_galleries.edit', ['project_id' => $project->id, 'id' => $result->id]) }}" class="action-icon -warning" data-toggle="tooltip" data-placement="bottom" title="Editar" data-original-title="Editar"><i class="icon-pencil"></i></a>
                      @endcan

                      @can('delete_projects_submodules')
                      <a href="" data-link="{{ route('admin.projects_galleries.destroy', ['project_id' => $project->id, 'id' => $result->id]) }}" class="action-icon js-confirm-delete-release -danger" data-toggle="tooltip" data-placement="bottom" data-title="{{ $result->title }}" title="Excluir" data-original-title="Excluir"><i class="icon-trash"></i></a>
                      @endcan
                    </div>
                  </div>
                  <div class="card-body">
                    @if($result->getUrl('thumb'))
                      <div class="release">
                        <img src="{{ $result->getUrl('thumb') }}" alt="{{ $result->title }}"> </a>
                      </div>
                    @endif
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <nav class="pull-right">
          <ul class="pagination rounded">
            {!! $results->render() !!}
          </ul>
        </nav>
      </div>
    </div>
  </div>
@endsection
