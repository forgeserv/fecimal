<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
  <div class="card review-card">
    <div class="card-header header-sm d-flex justify-content-between align-items-center blue">
      <h4 class="card-title"> <i class="icon-sm link-icon mdi mdi-pencil"></i> Detalhes</h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="form-group col-md-6 col-lg-6 col-sm-12">
          {!! Form::label('name', 'Nome', []) !!}
          {!! Form::text('name', old('name'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('name'))
            <span class="help-block">{{ $errors->first('name') }}</span>
          @endif
        </div>
        <div class="form-group col-md-12 col-lg-12 col-sm-12">
          {!! Form::label('body', 'Descrição', []) !!}
          {!! Form::textarea('body', isset($result) ? $result->body : old('body'), ['class' => 'form-control form-control-lg', 'rows' => '10']) !!}
          @if($errors->has('body'))
            <span class="help-block">{{ $errors->first('body') }}</span>
          @endif
        </div>
        <div class="col-md-12 col-lg-12 col-sm-12">
          <div class="icheck-flat">
            {!! Form::hidden('active', 0) !!}
            {!! Form::checkbox('active', true, isset($result) ? $result->active : false, ['class' => '', 'id' => 'active']) !!}
            {!! Form::label('active', 'Ativo?', []) !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
  @include('admin.partials.seo')
</div>


