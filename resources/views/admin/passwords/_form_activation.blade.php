{!! Form::open(['class' => 'form -default', 'route' => 'admin.activation.processCreate', 'method' => 'POST', 'novalidate', 'role' => 'form']) !!}
  <div class="form-group">
    {!! Form::label('password', 'Senha', ['class' => 'label sr-only']) !!}
    {!! Form::password('password', ['class' => 'form-control input', 'placeholder' => 'Senha']) !!}
    @if($errors->has('password'))
      <span class="help-block">{{ $errors->first('password') }}</span>
    @endif
  </div>
  <div class="form-group">
    {!! Form::label('password_confirmation', 'Confirmar a senha', ['class' => 'label sr-only']) !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control input', 'placeholder' => 'Confirmar a senha']) !!}
    @if($errors->has('password_confirmation'))
      <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
    @endif
  </div>
  <div class="form-action">
    {!! Form::button('Registrar', ['class' => 'button -solid -t30', 'type' => 'submit']) !!}
  </div>
{!! Form::close() !!}
