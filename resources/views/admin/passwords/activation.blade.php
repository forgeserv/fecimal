@extends('admin.layouts.public')

@section('content')
<section class="section -contact">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <header class="s-heading text-center">
          <h3 class="title f-majesti f-c30 f-bold f-s3">Primeiro acesso</h3>
          <p class="text -v1 f-c10">Cadastre uma senha de acesso</p>
        </header>
        <div class="s-content">
          @include('admin.passwords._form_activation')
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
