<div class="card">
  <div class="card-header header-sm d-flex justify-content-between align-items-center">
    <h4 class="card-title"> <i class="link-icon mdi mdi-av-timer"></i> Atividades recentes</h4>
  </div>
  <div class="card-body">
    <div class="shedule-list d-flex align-items-center justify-content-between mb-3">
      <h3>{{ $today->formatLocalized('%d %B %Y') }}</h3>
      <small>{{ $eventsToday->count() }}</small>
    </div>
    @foreach($eventsToday as $event)
      <div class="event relative border-bottom py-3">
        <a href="{{ route('admin.audits.show', ['id' => $event->id]) }}" class="hover-me" title="{{ $event->full_name }}"></a>
        <p class="mb-2 font-weight-medium">{{ $event->created_at->formatLocalized('Horário:  %H:%M') }}</p>
        <div class="d-flex align-items-center">
          <div class="badge badge-default">{{ $event->user->full_name }} fez {{ $event->action_name }}</div>
          <small class="text-muted ml-2">{{ $event->type_name }}</small>
          <div class="image-grouped ml-auto">
            <img src="{{ asset('/admin_assets/images/faces/default.png') }}" alt="profile image">
          </div>
        </div>
      </div>
    @endforeach
  </div>
</div>
