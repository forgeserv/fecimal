@extends('admin.layouts.default')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-12 grid-margin d-none d-lg-block">
      <div class="intro-banner">
        <div class="banner-image">
          <img src="{{ asset('/admin_assets/images/dashboard/banner_img.png') }}" alt="Bem-vindo">
        </div>
        <div class="content-area">
          <h3 class="mb-0">Bem-vindo, {{ auth()->user()->full_name }}</h3>
          <p class="mb-0">Este é o seu painel administrativo, onde você vai gerenciar todo o conteúdo do seu site e seus clientes.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    @can('view_messages')
      @if($messages->count() > 0)
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 grid-margin stretch-card">
          @include('admin.dashboard._messages')
        </div>
      @endif
    @endcan
    @can('view_activities')
      @if($eventsToday->count() > 0)
        <div class="col-md-4 col-lg-4 col-sm-4 grid-margin stretch-card">
          @include('admin.dashboard._activities')
        </div>
      @endif
    @endcan
  </div>
</div>
@endsection
