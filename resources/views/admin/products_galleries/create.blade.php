@extends('admin.layouts.default')

@section('content')
  <div class="content-wrapper">
    @include('flash::message')
    <div class="row">
      {{ Breadcrumbs::render('products_galleries.create', $product) }}
      <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
          <div class="col-12">
            <div class="card">
              <div class="card-body reset-padding margin-1">
                <div class="card-action-title">
                  <h3> Nova Galeria
                    <small class="text-muted"> Preencha os campos abaixo. </small>
                  </h3>
                </div>
                <div class="card-actions">
                  <div class="margin-btn">
                    <a href="{{ route('admin.products_galleries.index', ['id' => $product->id]) }}" class="btn btn-danger btn-lg" title="Voltar"><i class="fa fa-angle-left"></i>Voltar</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Selecione suas fotos</h4>
            {!! Form::open(['class' => 'dropzone d-flex align-items-center js-dropzone', 'method' => 'POST', 'route' => ['admin.products_galleries.store', 'id' => $product->id]]) !!}
              <div class="fallback">
                <input name="file" type="file" multiple placeholder="Selecione suas fotos"/>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
