<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
  <div class="card review-card">
    <div class="card-header header-sm d-flex justify-content-between align-items-center blue">
      <h4 class="card-title"> <i class="icon-sm link-icon mdi mdi-pencil"></i> Detalhes</h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="form-group col-md-6 col-lg-6 col-sm-12">
          {!! Form::label('title', 'Título', []) !!}
          {!! Form::text('title', old('title'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('title'))
            <span class="help-block">{{ $errors->first('title') }}</span>
          @endif
        </div>
        <div class="form-group col-md-6 col-lg-6 col-sm-12">
          {!! Form::label('subtitle', 'Subtítulo', []) !!}
          {!! Form::text('subtitle', old('subtitle'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('subtitle'))
            <span class="help-block">{{ $errors->first('subtitle') }}</span>
          @endif
        </div>
        <div class="form-group col-md-3 col-lg-3 col-sm-12">
          {!! Form::label('button', 'Botão', []) !!}
          {!! Form::text('button', old('button'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('button'))
            <span class="help-block">{{ $errors->first('button') }}</span>
          @endif
        </div>
        <div class="form-group col-md-6 col-lg-6 col-sm-12">
          {!! Form::label('refer', 'Redirecionar para', []) !!}
          {!! Form::text('refer', old('refer'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('refer'))
            <span class="help-block">{{ $errors->first('refer') }}</span>
          @endif
        </div>
        <div class="form-group col-md-3 col-lg-3 col-sm-12">
          {!! Form::label('target', 'Abrir', []) !!}
          {!! Form::select('target', [1 => 'Na mesma aba', 2 => 'Nova aba'], old('target'), ['class' => 'form-control form-control-lg', 'placeholder' => 'Selecione']) !!}
          @if($errors->has('target'))
            <span class="help-block">{{ $errors->first('target') }}</span>
          @endif
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12">
          <div class="icheck-flat">
            {!! Form::hidden('active', false) !!}
            {!! Form::checkbox('active', true, isset($result) ? $result->active : false, ['class' => '', 'id' => 'active']) !!}
            {!! Form::label('active', 'Ativo?', []) !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
  <div class="card review-card">
    <div class="card-header header-sm d-flex justify-content-between align-items-center red">
      <h4 class="card-title"> <i class="icon-sm link-icon mdi mdi-camera"></i> Medias</h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="form-group col-sm-12 col-lg-12 col-md-12">
          {!! Form::label('image_desktop', 'Desktop', ['']) !!}
          {!! Form::file('image_desktop', ['class' => 'file-upload-default']) !!}
          <div class="input-group col-xs-12">
            <input type="text" class="form-control file-upload-info" disabled="" placeholder="">
            <span class="input-group-append">
              <button class="file-upload-browse btn btn-default" type="button">Selecionar</button>
            </span>
          </div>
          @if($errors->has('image_desktop'))
            <span class="help-block">{{ $errors->first('image_desktop') }}</span>
          @endif
        </div>
        <div class="form-group col-sm-12 col-lg-12 col-md-12">
          {!! Form::label('image_mobile', 'Mobile', ['']) !!}
          {!! Form::file('image_mobile', ['class' => 'file-upload-default']) !!}
          <div class="input-group col-xs-12">
            <input type="text" class="form-control file-upload-info" disabled="" placeholder="">
            <span class="input-group-append">
              <button class="file-upload-browse btn btn-default" type="button">Selecionar</button>
            </span>
          </div>
          @if($errors->has('image_mobile'))
            <span class="help-block">{{ $errors->first('image_mobile') }}</span>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
