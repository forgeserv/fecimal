@extends('admin.layouts.default')

@section('content')
  <div class="content-wrapper">
    @include('flash::message')
    <div class="row">
      {{ Breadcrumbs::render('releases.index') }}
    </div>
    <div class="row">
      <div class="col-12 grid-margin">
        <div class="card card-statistics">
          <div class="row">
            @can('add_releases')
              <div class="card-col col-xl-12 col-lg-12 col-md-12 col-12">
                <div class="card-body">
                  <a href="{{ route('admin.releases.create') }}" class="hover-me" title="Adicionar"></a>
                  <div class="d-flex align-items-center justify-content-center flex-column flex-sm-row">
                    <i class="mdi mdi-account-plus text-success mr-0 mr-sm-4 icon-lg"></i>
                    <div class="wrapper text-center text-sm-left">
                      <p class="card-text mb-0">Adicionar</p>
                      <div class="fluid-container">
                        <h3 class="mb-0 font-weight-medium">Destaque</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endcan
          </div>
        </div>
      </div>
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Destaques</h4>
            <div class="row">
              @foreach($results as $result)
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                  <div class="card review-card m-bottom">
                    <div class="card-header header-sm d-flex justify-content-between align-items-center">
                      <h4 class="card-title">{{ $result->title }}</h4>
                      <div class="cards-actions">
                        @can('edit_releases')
                        <a href="{{ route('admin.releases.edit', ['id' => $result->id]) }}" class="action-icon" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Editar"><i class="icon-pencil"></i></a>
                        @endcan

                        @can('delete_releases')
                        <a data-link="{{ route('admin.releases.destroy', ['id' => $result->id]) }}" class="action-icon js-confirm-delete-release" data-toggle="tooltip" data-placement="bottom" data-title="{{ $result->title }}" title="" data-original-title="Excluir"><i class="icon-trash"></i></a>
                        @endcan
                      </div>
                    </div>
                    <div class="card-body">
                      @if($result->desktop)
                        <div class="release">
                          <img src="{{ $result->desktop }}" alt="{{ $result->title }}"> </a>
                        </div>
                      @endif
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
