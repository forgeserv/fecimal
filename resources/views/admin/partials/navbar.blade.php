 <nav class="navbar horizontal-layout col-lg-12 col-12 p-0">
    <div class="container d-flex flex-row nav-top">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top">
        <a class="navbar-brand brand-logo" href="{{ route('admin.dashboard.index') }}" title="Home">
          <img src="{{ asset('/assets/images/brand.svg') }}" alt="logo" /> </a>
        <a class="navbar-brand brand-logo-mini" href="{{ route('admin.dashboard.index') }}" title="Home">
          <img src="{{ asset('/assets /images/brand.svg') }}" alt="logo" /> </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">Olá,
              {{ auth()->user()->first_name }}
              <img class="img-xs rounded-circle ml-3" src="{{ auth()->user()->photo }}" alt="Profile image"> </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <a href="{{ route('admin.accounts.show') }}" class="dropdown-item mt-2" title="Perfil"> Perfil </a>
              <a href="{{ route('home.index') }}" target="_blank" class="dropdown-item mt-2" title="Ir ao site"> Ir ao site </a>
              <a href="{{ route('admin.session.logout') }}" class="dropdown-item" title="Sair"> Sair </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </div>
    <div class="nav-bottom">
      <div class="container">
        <ul class="nav page-navigation">
          <li class="nav-item {{ active('admin.dashboard.index', 'active') }}">
            <a href="{{ route('admin.dashboard.index') }}" class="nav-link" title="Dashboard">
              <i class="link-icon mdi mdi-airplay"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
     {{--      <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="link-icon mdi mdi-chart-line"></i>
              <span class="menu-title">Acessos</span>
            </a>
          </li> --}}
          <li class="nav-item {{ active('admin.messages.index', 'active') }}">
            <a href="{{ route('admin.messages.index') }}" class="nav-link" title="Mensagens">
              <i class="link-icon mdi mdi-comment-outline"></i>
              <span class="menu-title">Mensagens</span>
            </a>
          </li>
          <li class="nav-item {{ active('admin.audits.index', 'active') }}">
            <a href="{{ route('admin.audits.index') }}" class="nav-link" title="Auditoria">
              <i class="link-icon mdi mdi-equal-box"></i>
              <span class="menu-title">Auditoria</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="link-icon mdi mdi-cube-outline"></i>
              <span class="menu-title ">Site</span>
              <i class="menu-arrow "></i>
            </a>
            <div class="submenu">
              <ul class="submenu-item ">
                @foreach(config('panel.menu') as $menu)
                  @can($menu['permission'])
                  <li class="nav-item {{ active($menu['route'], 'active') }}">
                    <a class="nav-link" href="{{ route($menu['route']) }}" title="{{ $menu['name'] }}">{{ $menu['name'] }}</a>
                  </li>
                  @endcan
                @endforeach
              </ul>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
