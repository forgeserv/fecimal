<script src="{{ asset('/admin_assets/vendors/js/vendor.bundle.base.js') }}"></script>
<script src="{{ asset('/admin_assets/vendors/js/vendor.bundle.addons.js') }}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{ asset('/admin_assets/js/shared/off-canvas.js') }}"></script>
<script src="{{ asset('/admin_assets/js/shared/hoverable-collapse.js') }}"></script>
<script src="{{ asset('/admin_assets/js/shared/misc.js') }}"></script>
<script src="{{ asset('/admin_assets/js/shared/settings.js') }}"></script>
<script src="{{ asset('/admin_assets/js/shared/todolist.js') }}"></script>
<script src="{{ asset('/admin_assets/js/shared/tooltips.js') }}"></script>
<script src="{{ asset('/admin_assets/js/shared/iCheck.js') }}"></script>
<script src="{{ asset('/admin_assets/js/shared/file-upload.js') }}"></script>
<script src="{{ asset('/admin_assets/js-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>

<!-- CDNS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.min.js"></script>

<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{ asset('/admin_assets/js/mask.js') }}"></script>
<script src="{{ asset('/admin_assets/js/demo_2/script.js') }}"></script>
<!-- End custom js for this page-->

<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/pt.js"></script>
<script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>

<script src="{{ asset('/admin_assets/vendors/summernote/dist/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('/admin_assets/js/custom.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.14/sweetalert2.all.min.js"></script>

@stack('scripts')
