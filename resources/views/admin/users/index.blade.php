@extends('admin.layouts.default')

@section('content')
  <div class="content-wrapper">
    @include('flash::message')
    <div class="row">
      {{ Breadcrumbs::render('users.index') }}
    </div>
    <div class="row">
      <div class="col-12 grid-margin">
        <div class="card card-statistics">
          <div class="row">
            @can('add_users')
              <div class="card-col col-xl-4 col-lg-4 col-md-4 col-6">
                <div class="card-body">
                  <a href="{{ route('admin.users.create') }}" class="hover-me" title="Adicionar"></a>
                  <div class="d-flex align-items-center justify-content-center flex-column flex-sm-row">
                    <i class="mdi mdi-account-plus text-success mr-0 mr-sm-4 icon-lg"></i>
                    <div class="wrapper text-center text-sm-left">
                      <p class="card-text mb-0">Adicionar</p>
                      <div class="fluid-container">
                        <h3 class="mb-0 font-weight-medium">Usuário</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endcan
            @can('view_roles')
              <div class="card-col col-xl-4 col-lg-4 col-md-4 col-6">
                <div class="card-body">
                  <a href="{{ route('admin.roles.index') }}" class="hover-me"></a>
                  <div class="d-flex align-items-center justify-content-center flex-column flex-sm-row">
                    <i class="mdi mdi-account-multiple-outline text-warning mr-0 mr-sm-4 icon-lg"></i>
                    <div class="wrapper text-center text-sm-left">
                      <p class="card-text mb-0">Acessar</p>
                      <div class="fluid-container">
                        <h3 class="mb-0 font-weight-medium">Grupos</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endcan
            @can('view_permissions')
              <div class="card-col col-xl-4 col-lg-4 col-md-4 col-6">
                <div class="card-body">
                  <a href="{{ route('admin.permissions.index') }}" class="hover-me"></a>
                  <div class="d-flex align-items-center justify-content-center flex-column flex-sm-row">
                    <i class="mdi mdi-key text-danger mr-0 mr-sm-4 icon-lg"></i>
                    <div class="wrapper text-center text-sm-left">
                      <p class="card-text mb-0">Acessar</p>
                      <div class="fluid-container">
                        <h3 class="mb-0 font-weight-medium">Permissões</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endcan
          </div>
        </div>
      </div>
      <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                {!! Form::open(['route' => 'admin.users.index', 'method' => 'GET', 'novalidate', 'role' => 'form']) !!}
                  <div class="row">
                    <div class="form-group no-margin col-md-3 col-lg-3 col-sm-12">
                      {!! Form::text('nome', Request::get('nome'), ['class' => 'form-control form-control-lg', 'placeholder' => 'Nome']) !!}
                    </div>
                   <div class="form-group no-margin col-md-2 col-lg-3 col-sm-12">
                      {!! Form::text('email', Request::get('email'), ['class' => 'form-control form-control-lg', 'placeholder' => 'E-mail']) !!}
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      {!! Form::select('grupo', $roles, Request::get('grupo'), ['class' => 'form-control form-control-lg', 'placeholder' => 'Grupo']) !!}
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      <button type="button" class="btn btn-danger btn-lg btn-block"><i class="fa fa-times"></i></button>
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      <button type="submit" class="btn btn-success btn-lg btn-block"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Usuários</h4>
            <span class="legend">Total: {{ $results->total() }}</span>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th> Nome </th>
                  <th> E-mail </th>
                  <th> Grupo </th>
                  <th> Último acesso </th>
                  <th> Status </th>
                  <th> Ações </th>
                </tr>
              </thead>
              <tbody>
                @foreach($results as $result)
                  <tr>
                    <td class="py-1">
                      <img src="{{ $result->photo }}" alt="{{ $result->full_name }}">
                      {{ $result->full_name }}
                    </td>
                    <td> {{ $result->email }} </td>
                    <td> {!! $result->rolesName !!} </td>
                    <td> {{ $result->last_login }} </td>
                    <td> {!! $result->statusName !!} </td>
                    <td>
                      @can('edit_users')
                        <a href="{{ route('admin.users.edit', ['id' => $result->id]) }}" class="action-icon -warning" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Editar"><i class="icon-pencil"></i></a>
                      @endcan

                      @if($result->active)
                        @can('delete_users')
                        <a href="{{ route('admin.users.deactivate', ['id' => $result->id]) }}" class="action-icon -danger" data-toggle="tooltip" data-placement="bottom" title="Desativar" data-original-title="Desativar"><i class="icon-ban"></i></a>
                        @endcan
                      @else
                        @can('delete_users')
                          <a href="{{ route('admin.users.activate', ['id' => $result->id]) }}" class="action-icon -success" data-toggle="tooltip" data-placement="bottom" title="Ativar" data-original-title="Ativar"><i class="icon-power"></i></a>
                        @endcan
                      @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @include('admin.partials.pagination')
        </div>
      </div>
    </div>
  </div>
@endsection
