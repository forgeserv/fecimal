<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
  <div class="card review-card">
    <div class="card-header header-sm d-flex justify-content-between align-items-center blue">
      <h4 class="card-title"> <i class="icon-sm link-icon mdi mdi-pencil"></i> Detalhes</h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="form-group col-md-6 col-lg-6 col-sm-12">
          {!! Form::label('name', 'Nome', []) !!}
          {!! Form::text('name', old('name'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('name'))
            <span class="help-block">{{ $errors->first('name') }}</span>
          @endif
        </div>
        <div class="form-group col-md-6 col-lg-6 col-sm-12">
          {!! Form::label('refer', 'Link externo', []) !!}
          {!! Form::text('refer', old('refer'), ['class' => 'form-control form-control-lg', 'placeholder' => 'Ex: https://g1.globo.com/pb/paraiba']) !!}
          @if($errors->has('refer'))
            <span class="help-block">{{ $errors->first('refer') }}</span>
          @endif
        </div>
        <div class="form-group col-md-4 col-lg-4 col-sm-4">
          {!! Form::label('category_id', 'Categoria', []) !!}
          {!! Form::select('category_id', $categories, isset($result) ? $result->category_id : old('category_id'), ['class' => 'form-control form-control-lg', 'placeholder' => 'Selecione']) !!}
          @if($errors->has('category_id'))
            <span class="help-block">{{ $errors->first('category_id') }}</span>
          @endif
          @if($categories->count() == 0)
            <span class="help-block">Cadastre uma categoria antes, <a href="{{ route('admin.projects_categories.create') }}" title="Clique aqui">clique aqui</a> para cadastrar uma nova categoria.</span>
          @endif
        </div>
        <div class="col-md-12 col-lg-12 col-sm-12">
          <div class="icheck-flat">
            {!! Form::hidden('active', 0) !!}
            {!! Form::checkbox('active', true, isset($result) ? $result->active : false, ['class' => '', 'id' => 'active']) !!}
            {!! Form::label('active', 'Ativar?', []) !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
  <div class="card review-card">
    <div class="card-header header-sm d-flex justify-content-between align-items-center red">
      <h4 class="card-title"> <i class="icon-sm link-icon mdi mdi-camera"></i> Medias</h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="form-group col-sm-12 col-lg-12 col-md-12">
          {!! Form::label('cover', 'Foto', ['']) !!}
          {!! Form::file('cover', ['class' => 'file-upload-default']) !!}
          <div class="input-group col-xs-12">
            <input type="text" class="form-control file-upload-info" disabled="" placeholder="">
            <span class="input-group-append">
              <button class="file-upload-browse btn btn-default" type="button">Selecionar</button>
            </span>
          </div>
          @if($errors->has('cover'))
            <span class="help-block">{{ $errors->first('cover') }}</span>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
