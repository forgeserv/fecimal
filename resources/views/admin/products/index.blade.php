@extends('admin.layouts.default')

@section('content')
  <div class="content-wrapper">
    @include('flash::message')
    <div class="row">
      {{ Breadcrumbs::render('products.index') }}
    </div>
    <div class="row">
      <div class="col-12 grid-margin">
        <div class="card card-statistics">
          <div class="row">
             @can('add_products_submodules')
              <div class="card-col col-xl-6 col-lg-6 col-md-6 col-6">
                <div class="card-body">
                  <a href="{{ route('admin.products_categories.index') }}" class="hover-me" title="Listar"></a>
                  <div class="d-flex align-items-center justify-content-center flex-column flex-sm-row">
                    <i class="mdi mdi-tag text-danger mr-0 mr-sm-4 icon-lg"></i>
                    <div class="wrapper text-center text-sm-left">
                      <p class="card-text mb-0">Listar</p>
                      <div class="fluid-container">
                        <h3 class="mb-0 font-weight-medium">Categorias</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endcan
            @can('add_products')
              <div class="card-col col-xl-6 col-lg-6 col-md-6 col-6">
                <div class="card-body">
                  <a href="{{ route('admin.products.create') }}" class="hover-me" title="Adicionar"></a>
                  <div class="d-flex align-items-center justify-content-center flex-column flex-sm-row">
                    <i class="mdi mdi-account-plus text-success mr-0 mr-sm-4 icon-lg"></i>
                    <div class="wrapper text-center text-sm-left">
                      <p class="card-text mb-0">Adicionar</p>
                      <div class="fluid-container">
                        <h3 class="mb-0 font-weight-medium">Produto</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endcan
          </div>
        </div>
      </div>
      <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                {!! Form::open(['route' => 'admin.products.index', 'method' => 'GET', 'novalidate', 'role' => 'form']) !!}
                  <div class="row">
                    <div class="form-group no-margin col-md-6 col-lg-6 col-sm-12">
                      {!! Form::text('nome', Request::get('nome'), ['class' => 'form-control form-control-lg', 'placeholder' => 'Nome']) !!}
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      {!! Form::select('categoria', $categories, Request::get('categoria'), ['class' => 'form-control form-control-lg', 'placeholder' => 'Categoria']) !!}
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      <button type="button" class="btn btn-danger btn-lg btn-block"><i class="fa fa-times"></i></button>
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      <button type="submit" class="btn btn-success btn-lg btn-block"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Produtos</h4>
            <span class="legend">Total: {{ $results->total() }}</span>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th> Nome </th>
                  <th> Categoria </th>
                  <th> Status </th>
                  <th> Ações </th>
                </tr>
              </thead>
              <tbody>
                @foreach($results as $result)
                  <tr>
                    <td>
                      @if($result->cover)
                       <img src="{{ $result->cover }}" alt="{{ $result->name }}">
                      @endif
                      {{ $result->name }}
                     </td>
                     <td> {{ $result->category->name }}</td>
                    <td> {!! isActive($result->publish) !!} </td>
                    <td>
                      @can('edit_products')
                        <a href="{{ route('admin.products_galleries.index', ['id' => $result->id]) }}" class="action-icon -success" data-toggle="tooltip" data-placement="bottom" title="Galeria" data-original-title="Galeria"><i class="icon-camera"></i></a>
                      @endcan

                      @can('edit_products')
                        <a href="{{ route('admin.products.edit', ['id' => $result->id]) }}" class="action-icon -warning" data-toggle="tooltip" data-placement="bottom" title="Editar" data-original-title="Editar"><i class="icon-pencil"></i></a>
                      @endcan

                      @can('delete_products')
                        <a data-link="{{ route('admin.products.destroy', ['id' => $result->id]) }}" class="action-icon js-confirm-delete -danger" data-toggle="tooltip" data-placement="bottom" data-title="{{ $result->name }}" title="Excluir" data-original-title="Excluir"><i class="icon-trash"></i></a>
                      @endcan
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @include('admin.partials.pagination')
        </div>
      </div>
    </div>
  </div>
@endsection
