<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
  <div class="card review-card">
    <div class="card-header header-sm d-flex justify-content-between align-items-center blue">
      <h4 class="card-title"> <i class="icon-sm link-icon mdi mdi-pencil"></i> Detalhes</h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="form-group col-md-6 col-lg-6 col-sm-12">
          {!! Form::label('title', 'Título', []) !!}
          {!! Form::text('title', old('title'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('title'))
            <span class="help-block">{{ $errors->first('title') }}</span>
          @endif
        </div>
        <div class="form-group col-md-6 col-lg-6 col-sm-12">
          {!! Form::label('subtitle', 'Subtítulo', []) !!}
          {!! Form::text('subtitle', old('subtitle'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('subtitle'))
            <span class="help-block">{{ $errors->first('subtitle') }}</span>
          @endif
        </div>
        <div class="form-group col-md-4 col-lg-4 col-sm-12">
          {!! Form::label('legend', 'Legenda', []) !!}
          {!! Form::text('legend', old('legend'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('legend'))
            <span class="help-block">{{ $errors->first('legend') }}</span>
          @endif
        </div>
        <div class="form-group col-md-4 col-lg-4 col-sm-12">
          {!! Form::label('owner', 'Função', []) !!}
          {!! Form::text('owner', old('owner'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('owner'))
            <span class="help-block">{{ $errors->first('owner') }}</span>
          @endif
        </div>
        <div class="form-group col-md-4 col-lg-4 col-sm-12">
          {!! Form::label('architect', 'Arquiteto', []) !!}
          {!! Form::text('architect', old('architect'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('architect'))
            <span class="help-block">{{ $errors->first('architect') }}</span>
          @endif
        </div>
        <div class="form-group col-md-4 col-lg-4 col-sm-12">
          {!! Form::label('year', 'Ano', []) !!}
          {!! Form::text('year', old('year'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('year'))
            <span class="help-block">{{ $errors->first('year') }}</span>
          @endif
        </div>
        <div class="form-group col-md-4 col-lg-4 col-sm-12">
          {!! Form::label('link_extern', 'Link Externo', []) !!}
          {!! Form::text('link_extern', old('link_extern'), ['class' => 'form-control form-control-lg', 'placeholder' => 'Ex: https://www.youtube.com/watch?v=_PBlykN4KIY&list=RD8j741TUIET0&index=27']) !!}
          @if($errors->has('link_extern'))
            <span class="help-block">{{ $errors->first('link_extern') }}</span>
          @endif
        </div>
        <div class="form-group col-md-4 col-lg-4 col-sm-4">
          {!! Form::label('category_id', 'Categoria', []) !!}
          {!! Form::select('category_id', $categories, isset($result) ? $result->category_id : old('category_id'), ['class' => 'form-control form-control-lg', 'placeholder' => 'Selecione']) !!}
          @if($errors->has('category_id'))
            <span class="help-block">{{ $errors->first('category_id') }}</span>
          @endif
          @if($categories->count() == 0)
            <span class="help-block">Cadastre uma categoria antes, <a href="{{ route('admin.projects_categories.create') }}" title="Clique aqui">clique aqui</a> para cadastrar uma nova categoria.</span>
          @endif
        </div>
        <div class="form-group col-md-12 col-lg-12 col-sm-12">
          {!! Form::label('body', 'Descrição', []) !!}
          {!! Form::textarea('body', isset($result) ? $result->body : old('body'), ['class' => 'form-control form-control-lg', 'rows' => '10']) !!}
          @if($errors->has('body'))
            <span class="help-block">{{ $errors->first('body') }}</span>
          @endif
        </div>
        <div class="col-md-3 col-lg-3 col-sm-3">
          <div class="icheck-flat">
            {!! Form::hidden('publish', 0) !!}
            {!! Form::checkbox('publish', true, isset($result) ? $result->publish : false, ['class' => '', 'id' => 'publish']) !!}
            {!! Form::label('publish', 'Publicar?', []) !!}
          </div>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-3">
          <div class="icheck-flat">
            {!! Form::hidden('release', 0) !!}
            {!! Form::checkbox('release', true, isset($result) ? $result->release : false, ['class' => '', 'id' => 'release']) !!}
            {!! Form::label('release', 'Destaque?', []) !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
  <div class="card review-card bottom">
    <div class="card-header header-sm d-flex justify-content-between align-items-center red">
      <h4 class="card-title"> <i class="icon-sm link-icon mdi mdi-camera"></i> Medias</h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="form-group col-sm-12 col-lg-12 col-md-12">
          {!! Form::label('cover', 'Foto', ['']) !!}
          {!! Form::file('cover', ['class' => 'file-upload-default']) !!}
          <div class="input-group col-xs-12">
            <input type="text" class="form-control file-upload-info" disabled="" placeholder="">
            <span class="input-group-append">
              <button class="file-upload-browse btn btn-default" type="button">Selecionar</button>
            </span>
          </div>
          @if($errors->has('cover'))
            <span class="help-block">{{ $errors->first('cover') }}</span>
          @endif
        </div>

        <div class="form-group col-sm-12 col-lg-12 col-md-12">
          {!! Form::label('header', 'Topo', ['']) !!}
          {!! Form::file('header', ['class' => 'file-upload-default']) !!}
          <div class="input-group col-xs-12">
            <input type="text" class="form-control file-upload-info" disabled="" placeholder="">
            <span class="input-group-append">
              <button class="file-upload-browse btn btn-default" type="button">Selecionar</button>
            </span>
          </div>
          @if($errors->has('header'))
            <span class="help-block">{{ $errors->first('header') }}</span>
          @endif
        </div>
      </div>
    </div>
  </div>
  @include('admin.partials.seo')
</div>

