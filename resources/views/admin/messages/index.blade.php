@extends('admin.layouts.default')

@section('content')
  <div class="content-wrapper">
     @include('flash::message')
    <div class="row">
      {{ Breadcrumbs::render('messages.index') }}
    </div>
    <div class="row">
      <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                {!! Form::open(['route' => 'admin.messages.index', 'method' => 'GET', 'novalidate', 'role' => 'form']) !!}
                  <div class="row">
                    <div class="form-group no-margin col-md-4 col-lg-4 col-sm-12">
                      {!! Form::text('nome', Request::get('nome'), ['class' => 'form-control form-control-lg', 'placeholder' => 'Nome']) !!}
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      {!! Form::text('email', Request::get('email'), ['class' => 'form-control form-control-lg', 'placeholder' => 'E-mail']) !!}
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      {!! Form::text('recebidaem', Request::get('recebidaem'), ['class' => 'form-control form-control-lg js-datepicker', 'placeholder' => 'Recbida em']) !!}
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      <button type="button" class="btn btn-danger btn-lg btn-block"><i class="fa fa-times"></i></button>
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      <button type="submit" class="btn btn-success btn-lg btn-block"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Mensagens</h4>
            <span class="legend">Total: {{ $results->total() }}</span>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th> Remetente </th>
                  <th> E-mail </th>
                  <th> Assunto </th>
                  <th> Recebida em </th>
                  <th> Ação </th>
                </tr>
              </thead>
              <tbody>
                @foreach($results as $result)
                  <tr>
                    <td> {{ $result->name }} </td>
                    <td> {{ $result->email }} </td>
                    <td> {{ $result->subject }} </td>
                    <td> {{ $result->created_at->format('d/m/Y') }} </td>
                    <td>
                      <a href="{{ route('admin.messages.show', ['id' => $result->id]) }}" class="action-icon -success" data-toggle="tooltip" data-placement="bottom" title="Visualizar" data-original-title="Visualizar"><i class="icon-eye"></i></a>
                      @can('delete_messages')
                        <a href="" data-link="{{ route('admin.messages.destroy', ['id' => $result->id]) }}" class="action-icon js-confirm-delete -danger" data-toggle="tooltip" data-placement="bottom" data-title="{{ $result->name }}" title="Excluir" data-original-title="Excluir"><i class="icon-trash"></i></a>
                      @endcan
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @include('admin.partials.pagination')
        </div>
      </div>
    </div>
  </div>
@endsection
