<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 grid-margin stretch-card">
  <div class="card card-statistics social-card card-default">
    <div class="card-header header-sm">
      <div class="d-flex align-items-center">
        <div class="wrapper d-flex align-items-center media-info text-facebook">
          <i class="mdi mdi-account-circle icon-md"></i>
          <h2 class="card-title ml-3">Contato</h2>
        </div>
      </div>
    </div>
    <div class="card-body">
      <img class="d-block img-sm rounded-circle mx-auto mb-2" src="{{ asset('/admin_assets/images/faces/default.png') }}" alt="{{ $result->name }}">
      <p class="text-center user-name">{{ $result->name }}</p>
      <p class="text-center mb-2 comment">{{ $result->email }}</p>
      <p class="text-center mb-2 comment">{{ $result->subject }}</p>
      <small class="d-block mt-4 text-center posted-date">{{ $result->created_at->format('d/m/Y H:i') }}</small>
    </div>
    <div class="card-footer"></div>
  </div>
</div>

<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 grid-margin stretch-card">
  <div class="card card-statistics social-card card-default">
    <div class="card-header header-sm">
      <div class="d-flex align-items-center">
        <div class="wrapper d-flex align-items-center media-info text-facebook">
          <i class="mdi mdi-email-open-outline icon-md"></i>
          <h2 class="card-title ml-3">Mensagem</h2>
        </div>
      </div>
    </div>
    <div class="card-body">
     <div class="message-body">
       {{ $result->body }}
     </div>
    </div>
    <div class="card-footer"></div>
  </div>
</div>

