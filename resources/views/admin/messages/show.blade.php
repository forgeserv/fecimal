@extends('admin.layouts.default')

@section('content')
  <div class="content-wrapper">
    <div class="row">
      @include('flash::message')
      {{ Breadcrumbs::render('messages.show', $result) }}
      <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
          <div class="col-12">
            <div class="card">
              <div class="card-body reset-padding margin-1">
                <div class="card-action-title">
                  <h3> Visualizar Mensagem #{{ $result->id }}
                  </h3>
                </div>
                <div class="card-actions">
                  <div class="margin-btn">
                    <a href="{{ route('admin.messages.index') }}" class="btn btn-danger btn-lg" title="Voltar"><i class="fa fa-angle-left"></i>Voltar</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
     @include('admin.messages._form')
    </div>
  </div>
@endsection
