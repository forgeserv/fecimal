{!! Form::open(['class' => 'form -default', 'route' => 'admin.session.login', 'method' => 'POST', 'novalidate', 'role' => 'form']) !!}
  <div class="form-group">
    {!! Form::label('email', 'E-mail', ['class' => 'label sr-only']) !!}
    {!! Form::text('email', old('email'), ['class' => 'form-control input', 'placeholder' => 'E-mail']) !!}
    @if($errors->has('email'))
      <span class="help-block">{{ $errors->first('email') }}</span>
    @endif
  </div>
  <div class="form-group">
    {!! Form::label('password', 'Senha', ['class' => 'label sr-only']) !!}
    {!! Form::password('password', ['class' => 'form-control input', 'placeholder' => 'Senha']) !!}
    @if($errors->has('password'))
      <span class="help-block">{{ $errors->first('password') }}</span>
    @endif
  </div>
  <div class="form-action">
    {!! Form::button('Acessar', ['class' => 'button -solid -t30', 'type' => 'submit']) !!}
  </div>
  <div class="form-group d-flex justify-content-between">
    <a href="#" class="text-small forgot-password text-black">Esqueceu a senha?</a>
  </div>
{!! Form::close() !!}
