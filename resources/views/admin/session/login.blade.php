@extends('admin.layouts.public')

@section('content')
<section class="section -contact">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <header class="s-heading text-center">
          <h3 class="title f-majesti f-c30 f-bold f-s3">Painel Administrativo</h3>
          <p class="text -v1 f-c10">Preencha com seu e-mail e sua senha.</p>
        </header>
        <div class="s-content">
          @include('admin.session._form')
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
