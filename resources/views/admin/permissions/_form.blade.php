<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
  <div class="card review-card">
    <div class="card-header header-sm d-flex justify-content-between align-items-center sac">
      <h4 class="card-title"> <i class="icon-sm link-icon mdi mdi-pencil"></i> Detalhes</h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="form-group col-md-12 col-lg-12 col-sm-12">
          {!! Form::label('details', 'Permissão', []) !!}
          {!! Form::text('details', old('details'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('details'))
            <span class="help-block">{{ $errors->first('details') }}</span>
          @endif
        </div>
        <div class="form-group col-md-12 col-lg-12 col-sm-12">
          {!! Form::label('name', 'Chave', []) !!}
          {!! Form::text('name', old('name'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('name'))
            <span class="help-block">{{ $errors->first('name') }}</span>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
  <div class="card review-card">
    <div class="card-header header-sm d-flex justify-content-between align-items-center blue">
      <h4 class="card-title"> <i class="icon-sm link-icon mdi mdi-key"></i> Grupos</h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="form-group col-md-12 col-lg-12 col-sm-12">
          {!! Form::label('roles[]', 'Grupos', []) !!}
          {!! Form::select('roles[]', $roles, old('roles'), ['class' => 'form-control form-control-lg js-duallistbox', 'multiple']) !!}
          @if($errors->has('roles'))
            <span class="help-block">{{ $errors->first('roles') }}</span>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>

