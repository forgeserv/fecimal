@extends('front.partials.layout')

@section('content')

<section class="section -project js-gallery" data-module="Gallery">
  <header class="s-heading bg-cover" style="background-image: url('{{ $project->header }}');">
    <h2 class="sr-only">{{ $project->title }}</h2>
  </header>
  <div class="s-content">
    <div class="details">
      <div class="container">
        <div class="col-md-3">
          <p class="text -line-left">{{ $project->architect }} | <span class="f-majesti f-italic">{{ $project->owner }}</span></p>
        </div>
        @if($project->year)
        <div class="col-md-2">
          <p class="text -line-left">Ano do projeto <br> <span class="f-bold">{{ $project->year }}</span></p>
        </div>
        @endif
        <div class="col-md-6 col-md-offset-1">
          <p class="text f-majesti f-c10">{{ $project->body }}</p>
        </div>
      </div>
    </div>
    @if(isset($galleryFirstCol['imagens']))
    <div class="gallery">
      <div class="container">
        <div class="row">
          @foreach($galleryFirstCol['imagens'] as $photo)
            <div class="col-md-4 p-0">
              <article class="card -project">
                <a href="{{ $photo->getUrl() }}" id="lg-share" class="c-link" data-pinterest-text="Pin it1" data-tweet-text="share on twitter 1">
                  <img class="sr-only" src="{{ $photo->getUrl() }}" alt="{{ $photo->name }}">
                </a>
                <figure class="c-figure bg-cover" style="background-image: url('{{ $photo->getUrl() }}');"></figure>
                <header class="c-caption icon-more-before">
                </header>
              </article>
            </div>
          @endforeach
        </div>
        <div class="row">
        @foreach($gallerySecondCol->chunk(3) as $key => $projects)
           @if($key % 2 == 0)
            <div class="col-md-6 p-0">
            @foreach($projects as $photo)
              @if($loop->index == 0 || $loop->index == 2)
                <article class="card -project">
                <a href="{{ $photo->getUrl() }}" id="lg-share" class="c-link" data-pinterest-text="Pin it1" data-tweet-text="share on twitter 1">
                  <img class="sr-only" src="{{ $photo->getUrl() }}" alt="{{ $photo->name }}">
                </a>
                <figure class="c-figure bg-cover" style="background-image: url('{{ $photo->getUrl() }}');"></figure>
                <header class="c-caption icon-more-before">
                </header>
                </article>
              @endif
            @endforeach
            </div>
            @foreach($projects as $photo)
              @if($loop->index == 1)
                <div class="col-md-6 p-0">
                  <article class="card -project {{ ($loop->index == 1) ? '-big' : '' }}">
                  <a href="{{ $photo->getUrl() }}" id="lg-share" class="c-link" data-pinterest-text="Pin it1" data-tweet-text="share on twitter 1">
                      <img class="sr-only" src="{{ $photo->getUrl() }}" alt="{{ $photo->name }}">
                    </a>
                  <figure class="c-figure bg-cover" style="background-image: url('{{ $photo->getUrl() }}');"></figure>
                  <header class="c-caption icon-more-before">
                  </header>
                  </article>
                </div>
              @endif
            @endforeach
          @else
            @foreach($projects as $photo)
              @if($loop->first)
                <div class="col-md-6 p-0">
                  <article class="card -project {{ ($loop->first) ? '-big' : '' }}">
                   <a href="{{ $photo->getUrl() }}" id="lg-share" class="c-link" data-pinterest-text="Pin it1" data-tweet-text="share on twitter 1">
                      <img class="sr-only" src="{{ $photo->getUrl() }}" alt="{{ $photo->name }}">
                    </a>
                    <figure class="c-figure bg-cover" style="background-image: url('{{ $photo->getUrl() }}');"></figure>
                    <header class="c-caption icon-more-before">
                    </header>
                  </article>
                </div>
              @endif
            @endforeach
             <div class="col-md-6 p-0">
              @foreach($projects as $photo)
                @if($loop->index == 1 || $loop->index == 2)
                  <article class="card -project">
                   <a href="{{ $photo->getUrl() }}" id="lg-share" class="c-link" data-pinterest-text="Pin it1" data-tweet-text="share on twitter 1">
                      <img class="sr-only" src="{{ $photo->getUrl() }}" alt="{{ $photo->name }}">
                    </a>
                    <figure class="c-figure bg-cover" style="background-image: url('{{ $photo->getUrl() }}');"></figure>
                    <header class="c-caption icon-more-before">
                    </header>
                  </article>
                @endif
              @endforeach
            </div>
          @endif
        @endforeach
        </div>
      </div>
    </div>
    @endif
  </div>
</section>

@stop
