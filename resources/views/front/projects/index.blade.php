@extends('front.partials.layout')

@section('content')

@if($projects->count() > 0)
  <section class="section -projects">
    <header class="s-heading"></header>
    <div class="s-content">
      <div class="container">
        <div class="row">
          @foreach($projects->chunk(3) as $key => $projects)
            @include('front.cards._projects', ['projects' => $projects, 'key' => $key])
          @endforeach
        </div>
        {{-- @if($portfolioCompleted)
          <div class="row text-center mt-40">
            <a href="{{ $portfolioCompleted }}" target="_blank" class="button -inline -t30 icon-download-after -has-icon-right" title="Portfólio Completo">Portfólio Completo</a>
          </div>
        @endif --}}
      </div>
    </div>
  </section>
@endif

@stop
