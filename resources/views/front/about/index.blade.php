@extends('front.partials.layout')

@section('content')

<section class="section -about">
  <div class="container">
    <div class="row">
      <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0">
        <header class="s-heading">
          <h2 class="title f-majesti f-c30 f-s3">Há 30 anos referência em qualidade e design.</h2>
        </header>
        <div class="s-content">
          <p class="text -v2">A FECIMAL ARTE & MADEIRA, empresa familiar, foi fundada em Agosto de 1993, tornando-se com o tempo uma das principais referências no mercado de fabrição de esquadrias da região.</p>
          <p class="text -v2">Produzimos: Cobertas, Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.
          <p class="text -v2">
          Toda a nossa linha é produzida em IPÊ, CUMARU e MAÇARANDUBA - madeiras de alta resistência e durabilidade - permitindo assim um excelente acabamento, tornando nossos produtos verdadeiras obras de arte.</p>
          <p class="text -v2">
          Do projeto ao acabamento final, a FECIMAL Arte & Madeira oferece ao seus clientes um atendimento diferenciado, onde o produto fabricado adequa-se ao projeto, garantindo com isso a qualidade desejada.</p>

          <p class="text -v2">A FECIMAL Arte & Madeira é uma empresa certificada pelo IBAMA.</p>
        </div>
      </div>
      <div class="col-sm-5 absolute-bg bg-cover" style="background-image: url('{{ asset('/assets/images/about2.png') }}');"></div>
    </div>
  </div>
</section>

<section class="section -video">
  <h4 class="sr-only">Vídeo de apresentação</h4>
  <div class="player" data-module="Player">
    <div id="player" data-plyr-provider="youtube" data-plyr-embed-id="x5FRT9sf500">
      <div class="play icon-play-before"></div>
    </div>
  </div>
</section>
@endsection
