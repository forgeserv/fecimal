@extends('front.partials.layout')

@section('content')
  {{-- @switch($category->slug)
    @case('produtos')
      @include('front.products._products', ['category' => $category])
    @break
    @case('madeiras')
      @include('front.products._woods', ['category' => $category])
    @break
    @case('ferragens')
      @include('front.products._irons', ['category' => $category])
    @break
  @endswitch --}}
     <section class="section -basic -partners text-center">
      <div class="container">
        <section class="s-heading">
          <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-offset-3 col-sm-6">
              <h2 class="title f-majesti f-c30 f-s3">Nossos parceiros</h2>
              <p class="text f-majesti f-s5 f-c16">A Fecimal entende que o sucesso de qualquer empresa não se faz
                sozinho e que o trabalho em parceria é fundamental para alavancar resultados positivos.</p>
            </div>
          </div>
        </section>
        <div class="s-content">
          <div class="row">
            @foreach($partners as $partner)
              <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                <article class="card -partner">
                  <a href="{{ $partner->link }}" target="{{ $partner->target }}" title="{{ $partner->name }}" class="c-link"></a>
                  <figure class="c-figure bg-contain" style="background-image: url('{{ $partner->photo }}');"></figure>
                </article>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </section>
@stop
