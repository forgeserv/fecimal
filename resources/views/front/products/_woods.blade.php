<section class="section -basic -hoods text-center" >
  <div class="container">
    <section class="s-heading">
      <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-offset-3 col-sm-6">
          <h2 class="title f-majesti f-c30 f-s3">{{ $category->name }}</h2>
          <p class="text f-majesti f-s5 f-c16">{{ $category->body }}</p>
        </div>
      </div>
    </section>
    <div class="s-content">
      <div class="row">
        @foreach($category->products as $product)
          <div class="col-xs-10 col-xs-offset-1 col-sm-offset-0 col-sm-4">
            <article class="card -item-legended">
              <figure class="c-figure bg-cover" style="background-image: url('{{ $product->cover }}');"></figure>
              <header class="c-caption">
              <h5 class="f-bold f-s5 f-majesti">{{ $product->name }}</h5>
            </header>
            </article>
          </div>
        @endforeach
      </div>
    </div>
  </div>
</section>
