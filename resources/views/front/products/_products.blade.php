<section class="section -basic -products text-center">
  <div class="container">
    <section class="s-heading">
      <h2 class="title f-majesti f-c20 f-s3 f-c30">{{ $category->name }}</h2>
      <p class="text -v1 f-majesti f-c16">{{ $category->body }}</p>
    </section>
    <div class="s-content">
      <div class="display">
        <div class="row">
          <aside class="col-md-3">
            <ul class="list -lined">
              @foreach($category->products as $product)
                <li class="item">
                  <a href="{{ route('products.index', ['slug' => $product->category->slug, 'product' => $product->slug]) }}"
                    class="{{ (Request::is('produtos/' . $category->slug. '/' . $product->slug) ) ? '-active' : '' }}" title="{{ $product->name }}">{{ $product->name }}</a>
                </li>
              @endforeach
            </ul>
          </aside>
          @if(!is_null($productFilter))
            <div class="slide js-swiper col-md-9 col-md-offset-3 p-0">
              <div class="slide-pagination -v2">
               <div class="swiper-button-prev icon-circle-left-before"></div>
                <div class="swiper-button-next icon-circle-right-before"></div>
              </div>
              <div class="wrapper">
                @foreach($productFilter->gallery as $gallery)
                  <div class="slide-item">
                    <article class="card -photo-gallery">
                      <figure class="c-figure bg-cover" style="background-image: url('{{ $gallery->getUrl() }}');"></figure>
                      <header class="c-caption">
                        <h6 class="title">{{ $gallery->name }}</h6>
                      </header>
                    </article>
                  </div>
               @endforeach
              </div>
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
