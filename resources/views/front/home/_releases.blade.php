@if($releases->count() > 0)
  <section class="section -welcome">
    <header>
      <h2 class="sr-only">Fecimal - Arte & Madeira</h2>
    </header>
    <div class="slide js-swiper" data-navigation-target=".-slide-preview">
      <div class="slide-counter visible-xs visible-sm"></div>
      <div class="wrapper">
        @foreach($releases as $release)
          @include('front.cards._releases', ['release' => $release])
        @endforeach
      </div>
      <div class="slider-actions hidden-xs hidden-sm">
        <ul class="list -slide-preview">
          @foreach($releases as $release)
            <li class="item">
              <a href="" class="link {{ $loop->first ? '-active' : '' }}" data-slide="{{ $loop->index }}">
                <span class="index">0{{ $loop->index + 1}}</span>
                <div class="legend">{{ $release->title }}</div>
              </a>
            </li>
          @endforeach
        </ul>
      </div>
    </div>
  </section>
@endif
