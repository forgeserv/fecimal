<section class="section -home-about bg-cover" style="background-image: url('{{ asset('/assets/images/about.png') }}');">
  <div class="container">
    <div class="row">
      <div class="col-xs-10 col-xs-offset-1 col-sm-offset-0 col-sm-8">
        <header class="s-heading text-left">
          <div class="uptitle f-bebas f-c30 f-s4 f-ls3 f-bold">30 anos de histórias</div>
          <h3 class="title f-majesti f-c20 f-s3 f-bold">Realizando Sonhos</h3>
          <p class="text -v1 f-c16"> A Fecimal arte e madeira  atua na fabricação de fábrica de esquadrias , Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.
Executamos projetos arquitetônicos com todo o carinho e cuidado buscando a entrega de um produto de excelente qualidade para o lar de nossos clientes.
Além de termos uma grande estruturas, equipamentos de primeira e colaboradores capacitados, somos uma empresa certificada pelo IBAMA.</p>
          <a href="{{ route('about.index') }}" class="button -basic -t30 -has-icon-right icon-right-before" title="Ver mais">Ver mais</a>
        </header>
      </div>
    </div>
  </div>
</section>
