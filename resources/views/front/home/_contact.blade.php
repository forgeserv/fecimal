<section class="section -contact">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <header class="s-heading text-center">
          <h3 class="title f-majesti f-c30 f-bold f-s3">Fale Conosco</h3>
          <p class="text -v1 f-c10">Entre em contato conosco diretamente pelo e-mail: <a href="mailto:contato@fecimal.com.br" class="f-italic">contato@fecimal.com.br</a> <br> ou SAC: <a href="tel:558332465297" class="f-italic">(83) 3246.5297</a></p>
          <p class="text -v1 f-c10">Se preferir, preencha o formulário abaixo:</p>
        </header>
        <div class="s-content">
          {!! Form::open(['class' => 'form -default', 'novalidate', 'method' => 'post', 'route' => 'api.message.store']) !!}
            <div class="form-group">
              {!! Form::label('name', 'Seu nome', ['class' => 'label sr-only']) !!}
              {!! Form::text('name', old('name'), ['class' => 'form-control input', 'placeholder' => 'Nome']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('email', 'E-mail', ['class' => 'label sr-only']) !!}
              {!! Form::text('email', old('email'), ['class' => 'form-control input', 'placeholder' => 'E-mail']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('phone', 'Telefone', ['class' => 'label sr-only']) !!}
              {!! Form::text('phone', old('phone'), ['class' => 'form-control input', 'placeholder' => 'Telefone']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('email', 'Um pouco sobre você/sua empresa', ['class' => 'label sr-only']) !!}
              {!! Form::textarea('body', old('message'), ['class' => 'form-control textarea', 'rows' => '4']) !!}
            </div>
            <div class="form-action">
              {!! Form::button('Enviar', ['class' => 'button -solid -t30 js-send-form', 'type' => 'submit']) !!}
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</section>
