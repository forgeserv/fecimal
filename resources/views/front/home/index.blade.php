@extends('front.partials.layout')

@section('content')
@include('front.home._releases')
@include('front.home._projects')
@include('front.home._about')
@include('front.home._contact')
@endsection
