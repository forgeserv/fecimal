@if($projects->count() > 0)
<section class="section -features">
  <div class="container">
    <div class="slide js-swiper" data-pagination="fraction">
      <div class="wrapper">
        @foreach($projects as $project)
          @include('front.cards._projects_home', ['project' => $project])
        @endforeach
      </div>
      <div class="slide-action">
        <div class="slide-pagination">
          <div class="slide-counter"></div>
          <div class="swiper-button-prev icon-left-before"></div>
          <div class="swiper-button-next icon-right-before"></div>
        </div>
      </div>
    </div>
  </div>
</section>
@endif
