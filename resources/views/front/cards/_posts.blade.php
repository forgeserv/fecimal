<article class="card -post text-left">
  <a href="{{ route('posts.show', ['category' => $post->category->slug, 'slug' => $post->slug]) }}" class="c-link" title="{{ $post->title }}"></a>
  <div class="row">
    @if($post->cover)
      <div class="col-sm-6 {{ ($loop->index % 2 == 1) ? 'col-md-push-6' : ''  }}">
        <figure class="c-figure bg-cover" style="background-image: url('{{ $post->thumb }}');"></figure>
      </div>
    @endif
    <div class="col-sm-6 {{ ($loop->index % 2 == 1) ? 'col-md-pull-6' : ''  }}">
      <header class="c-caption">
        <h3 class="title f-c16 f-majesti">{{ $post->title }}</h3>
        <p class="text f-c16">{{ $post->subtitle }}</p>
      </header>
    </div>
  </div>
</article>
