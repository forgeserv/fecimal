@if($key % 2 == 0)
  <div class="col-md-6 p-0">
  @foreach($projects as $project)
    @if($loop->index == 0 || $loop->index == 2)
      <article class="card -project">
        <a href="{{ route('projects.show', ['category' => $project->category->slug, 'slug' => $project->slug]) }}" class="c-link" title="{{ $project->title }}" data-index="{{ $loop->index }}"></a>
        @if($project->cover)
        	<figure class="c-figure bg-cover" style="background-image: url('{{ $project->cover }}');"></figure>
        @endif
        <header class="c-caption">
          <div class="wrap">
            <h4 class="title f-majesti f-c10">{{ $project->architect }}</h4>
            @if($project->owner)
            <p class="legend icon-circle-right-before">{{ $project->owner }}</p>
            @endif
          </div>
        </header>
      </article>
    @endif
  @endforeach
  </div>
  @foreach($projects as $project)
    @if($loop->index == 1)
      <div class="col-md-6 p-0">
        <article class="card -project {{ ($loop->index == 1) ? '-big' : '' }}">
          <a href="{{ route('projects.show', ['category' => $project->category->slug, 'slug' => $project->slug]) }}" class="c-link" title="{{ $project->title }}" data-index="{{ $loop->index }}"></a>
          @if($project->cover)
            <figure class="c-figure bg-cover" style="background-image: url('{{ $project->cover }}');"></figure>
          @endif
          <header class="c-caption">
            <div class="wrap">
              <h4 class="title f-majesti f-c10">{{ $project->architect }}</h4>
              @if($project->owner)
              <p class="legend icon-circle-right-before">{{ $project->owner }}</p>
              @endif
            </div>
          </header>
        </article>
      </div>
    @endif
  @endforeach
@else
  @foreach($projects as $project)
    @if($loop->first)
      <div class="col-md-6 p-0">
        <article class="card -project {{ ($loop->first) ? '-big' : '' }}">
          <a href="{{ route('projects.show', ['category' => $project->category->slug, 'slug' => $project->slug]) }}" class="c-link" title="{{ $project->title }}" data-index="{{ $loop->index }}"></a>
          @if($project->cover)
            <figure class="c-figure bg-cover" style="background-image: url('{{ $project->cover }}');"></figure>
          @endif
          <header class="c-caption">
            <div class="wrap">
              <h4 class="title f-majesti f-c10">{{ $project->architect }}</h4>
              @if($project->owner)
              <p class="legend icon-circle-right-before">{{ $project->owner }}</p>
              @endif
            </div>
          </header>
        </article>
      </div>
    @endif
  @endforeach
   <div class="col-md-6 p-0">
    @foreach($projects as $project)
      @if($loop->index == 1 || $loop->index == 2)
        <article class="card -project">
          <a href="{{ route('projects.show', ['category' => $project->category->slug, 'slug' => $project->slug]) }}" class="c-link" title="{{ $project->title }}" data-index="{{ $loop->index }}"></a>
          @if($project->cover)
            <figure class="c-figure bg-cover" style="background-image: url('{{ $project->cover }}');"></figure>
          @endif
          <header class="c-caption">
            <div class="wrap">
              <h4 class="title f-majesti f-c10">{{ $project->architect }}</h4>
              @if($project->owner)
              <p class="legend icon-circle-right-before">{{ $project->owner }}</p>
              @endif
            </div>
          </header>
        </article>
      @endif
    @endforeach
  </div>
@endif
