<div class="slide-item">
 <article class="card -feature text-right">
    <div class="row">
      @if($project->crop)
      <div class="col-md-6">
        <figure class="c-figure">
          <img src="{{ $project->crop }}" alt="{{ $project->title }}">
        </figure>
      </div>
      @endif
      <div class="col-md-6">
        <header class="c-caption">
          <div class="uptitle f-bebas f-c30 f-s4 f-ls3 f-bold">{{ $project->category->name }}</div>
          <h3 class="title f-majesti f-c20 f-s3">{{ $project->title }}</h3>
          <p class="text -v1 f-c16">{{ $project->body }}</p>
          @if($project->link_extern)
            <a href="{{ $project->link_extern }}" target="_blank" class="button -basic -t30 -has-icon-right icon-right-before" title="Ver mais">Ver mais</a>
          @endif
        </header>
      </div>
    </div>
  </article>
</div>
