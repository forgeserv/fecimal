<div class="slide-item">
  <article class="card -intro">
    @if($release->desktop)
      <div class="c-figure bg-cover" style="background-image: url('{{ $release->desktop }}');"></div>
    @endif
    <div class="container">
      <header class="c-caption">
        <h2 class="title f-majesti f-c10">{{ $release->title }}</h2>
        <p class="text -v1 f-c10">{{ $release->subtitle }}</p>
        @if($release->refer)
          <a href="{{ $release->refer }}" target="{{ $release->target }}" class="button -basic -t10 -has-icon-left icon-plus-before" title="{{ $release->button }}">{{ $release->button }}</a>
        @endif
      </header>
    </div>
  </article>
</div>
