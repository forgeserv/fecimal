@extends('front.partials.layout')

@section('content')
  @if($posts->count() > 0)
    <section class="section -basic -blog text-center">
      <div class="container">
        <section class="s-heading">
          <h2 class="sr-only">Blog</h2>
        </section>
        <div class="s-content">
          @foreach($posts as $post)
            @include('front.cards._posts', ['post' => $post])
          @endforeach
        </div>
      </div>
    </section>
  @endif
@stop


