@extends('front.partials.layout')

@section('content')

<section class="section -page -blog-post">
  <div class="container">
    <div class="row">
      <div class="col-xs-10 col-xs-offset-1 col-sm-offset-2 col-sm-8">
        <header class="s-heading">
          <div class="row">
            <div class="col-md-12 text-center">
              <time datetime="2018-08-20" class="date f-c20">{{ $post->date_publish->format('d/m/Y') }}</time>
            </div>
          </div>
          <h2 class="title f-s3 f-bold text-center f-c30">{{ $post->title }}</h2>
          <p class="legend">{{ $post->subtitle }}</p>
        </header>
        <div class="s-content post">
          {!! $post->body !!}
        </div>
        <div class="share text-center">
          <span class="legend f-uppercase f-s6 f-bold f-c20 f-upper">Compartilhe</span>
          @include('front.partials.share', ['title' => $post->title, 'subtitle' => $post->subtitle, 'refer' => route('posts.show', ['category' => $post->category->slug, 'slug' => $post->slug]),
        ])
        </div>
      </div>
    </div>
  </div>
</section>

@stop
