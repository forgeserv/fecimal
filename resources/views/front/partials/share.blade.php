<ul class="list -social -inline -rounded -colors">
  <li class="item">
     <a href="#" data-provider="facebook" data-title="{{ $title }}" data-subtitle="{{ $subtitle }}" data-refer="{{ $refer }}" class="js-share link icon-facebook-before" title="Facebook">
     <span class="sr-only">Facebook</span>
     </a>
  </li>
  <li class="item">
     <a href="#" data-provider="whatsapp" data-title="{{ $title }}" data-subtitle="{{ $subtitle }}" data-refer="{{ $refer }}" class="js-share link icon-whatsapp-before" title="WhatsApp">
     <span class="sr-only">Whatsapp</span>
     </a>
  </li>
</ul>
