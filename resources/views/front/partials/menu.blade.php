<nav class="menu js-menu">
  <h3 class="container sr-only">Menu</h3>
  <div class="menu-principal">
    <div class="container">
      <span class="sr-only">Menu Principal</span>
      <div class="brand">
        <a href="{{ route('home.index') }}" title="Fecimal">
          <img class="logo" src="{{ asset('/assets/images/brand.svg') }}" alt="Fecimal">
        </a>
      </div>
      <ul class="list -principal">
        <li class="item">
          <a href="{{ route('home.index') }}" class="link {{ active('home.index', '-active') }}" title="Home">Home</a>
        </li>
         <li class="item">
          <a href="{{ route('products.index') }}" class="link {{ active('products.index', '-active') }}" title="Parceiros">Parceiros</a>
        </li>
      {{--   <li class="item -has-submenu">
          <a class="link icon-down-after" title="Produtos">Produtos</a>
          @if($categoriesMenu->count() > 0)
            <ul class="submenu">
              @foreach($categoriesMenu as $category)
                <li class="item">
                  <a href="{{ route('products.index', ['category' => $category->slug]) }}" class="link" title="{{ $category->name }}">{{ $category->name }}</a>
                </li>
              @endforeach
            </ul>
          @endif
        </li> --}}
        <li class="item">
          <a href="{{ route('projects.index') }}" class="link {{ active(['projects.index', 'projects.show'], '-active') }}" title="Projetos">Projetos</a>
        </li>
        <li class="item">
          <a href="{{ route('about.index') }}" class="link {{ active('about.index', '-active') }}" title="Sobre">Sobre</a>
        </li>
        <li class="item">
          <a href="{{ route('contact.index') }}" class="link {{ active('contact.index', '-active') }}" title="Contato">Contato</a>
        </li>
       {{--  <li class="item">
          <a href="{{ route('posts.index') }}" class="link {{ active('posts.index', '-active') }}" title="Blog">Blog</a>
        </li> --}}
      </ul>
    </div>
  </div>
</nav>
