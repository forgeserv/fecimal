<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {!! SeoTools::generate() !!}

    @include('front.partials.favicons')

    @include('front.partials.styles')
  </head>
  <body class="body bg-cover -{{ controller_name(null, false) }}" data-module="Common" id="main">
    @include('front.partials.header')

    <main class="main" role="main">
      @yield('content')
    </main>

    @include('front.partials.footer')

    @include('front.partials.scripts')

    @include('front.partials.analytics')
  </body>
</html>
