<footer class="footer -v1">
  {{-- <div class="container">
    <div class="row">
      @if($factoryGroup->count() > 0)
        <div class="col-md-5 p-0 col-md-offset-1">
          <div class="block -left -border">
            <div class="title f-bebas f-c30 f-s5 f-upper">Empresa do Grupo</div>
            <ul class="list -img-link">
              @foreach($factoryGroup as $factory)
                <li class="item">
                  <a href="{{ $factory->link }}" target="{{ $factory->target }}" class="link bg-contain" title="{{ $factory->name }}" style="background-image: url('{{ $factory->photo }}');"></a>
                </li>
              @endforeach
            </ul>
          </div>
        </div>
      @endif
      @if($factoryExcluded->count() > 0)
        <div class="col-md-3 p-0">
          <div class="block -center -border">
            <div class="title f-bebas f-c30 f-s5 f-upper">Revendedor Exclusivo</div>
            <ul class="list -img-link">
              @foreach($factoryExcluded as $factory)
                <li class="item">
                  <a href="{{ $factory->link }}" target="{{ $factory->target }}" class="link bg-contain" title="{{ $factory->name }}" style="background-image: url('{{ $factory->photo }}');"></a>
                </li>
              @endforeach
            </ul>
          </div>
        </div>
      @endif
      @if($factoryCertificate->count() > 0)
        <div class="col-md-3 p-0">
          <div class="block -center">
            <div class="title f-bebas f-c30 f-s5 f-upper">Empresa Certificada</div>
            <ul class="list -img-link">
              @foreach($factoryCertificate as $factory)
                <li class="item">
                  <a href="{{ $factory->link }}" target="{{ $factory->target }}" class="link bg-contain" title="{{ $factory->name }}" style="background-image: url('{{ $factory->photo }}');"></a>
                </li>
              @endforeach
            </ul>
          </div>
        </div>
      @endif
    </div>
  </div> --}}
  <div class="container copy">
    <div class="row m-0">
      <div class="col-md-5 text-left">
        <ul class="list -social -inline -t30">
          <li class="item">
            <a href="#" class="link icon-whatsapp-before">
              <span class="sr-only">Whatsapp</span>
            </a>
          </li>
          <li class="item">
            <a href="https://www.instagram.com/grupofecimal/" class="link icon-instagram-before">
              <span class="sr-only">Instagram</span>
            </a>
          </li>
          <br>
          <span class="f-c20 -sac">Endereço: R. Genival de Oliveira, 13, Parque Esperança, Cabedelo 58108-628</span>
        </ul>
        <span class="f-c20 -sac"><a class="f-bold f-c20" href="tel:558332465297">(83) 3246.5297</a></span>
      </div>
      <div class="col-xs-10 col-xs-offset-1 col-sm-offset-0 col-sm-7">
        <small>© Copyright FECIMAL Arte & Madeira 2019 - Todos os direitos reservados.</small>
        <a href="#main" class="button -only-icon -circle icon-up-before js-scroll-to"></a>
      </div>
    </div>
  </div>
</footer>
