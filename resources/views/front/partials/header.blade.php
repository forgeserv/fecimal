<header id="js-header" class="header -transparent">
  <div class="h-mobile">
    <div class="brand">
      <a href="{{ route('home.index') }}" title="Fecimal">
        <img class="logo" src="{{ asset('/assets/images/brand.svg') }}" alt="Fecimal">
      </a>
    </div>
    <button class="toggle -header js-toggle" data-target=".menu" type="button">
      <span class="bar">
      </span>
    </button>
  </div>
  @include('front.partials.menu')
</header>