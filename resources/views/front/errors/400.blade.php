@extends('front.partials.layout')

@section('content')

<section class="section -welcome">
  <header>
    <h2 class="sr-only">Fecimal - Arte & Madeira</h2>
  </header>
  <article class="card -intro">
    <div class="c-figure bg-cover" style="background-image: url('  {{ asset('/assets/images/2.png') }}');"></div>
    <div class="container">
      <header class="c-caption">
        <h2 class="title f-majesti f-c10">Página não <br> encontrada</h2>
        <a href="{{ route('home.index') }}" class="button -basic -t10 -has-icon-left icon-left-before" title="Voltar">Voltar</a>
      </header>
    </div>
  </article>
</section>

@stop
