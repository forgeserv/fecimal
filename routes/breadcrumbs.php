<?php

Breadcrumbs::for('dashboard', function ($breadcrumb) {
  $breadcrumb->push('Dashboard', route('admin.dashboard.index'));
});

Breadcrumbs::for('audits.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Auditoria', route('admin.audits.index'));
});

Breadcrumbs::for('accounts.show', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Meu Perfil', route('admin.accounts.show'));
});

Breadcrumbs::for('accounts.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('accounts.show');
  $breadcrumb->push('Editar Perfil', route('admin.accounts.edit', ['id' => $result->id]));
});

Breadcrumbs::for('audits.show', function ($breadcrumb, $result) {
  $breadcrumb->parent('audits.index');
  $breadcrumb->push('Visualizar Log', route('admin.audits.show', ['id' => $result->id]));
});

Breadcrumbs::for('messages.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Mensagens', route('admin.messages.index'));
});

Breadcrumbs::for('messages.show', function ($breadcrumb, $result) {
  $breadcrumb->parent('messages.index');
  $breadcrumb->push('Visualizar Mensagem', route('admin.messages.show', ['id' => $result->id]));
});

Breadcrumbs::for('users.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Usuários', route('admin.users.index'));
});

Breadcrumbs::for('users.create', function ($breadcrumb) {
  $breadcrumb->parent('users.index');
  $breadcrumb->push('Novo Usuário', route('admin.users.create'));
});

Breadcrumbs::for('users.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('users.index');
  $breadcrumb->push('Editar Usuário', route('admin.users.edit', ['id' => $result->id]));
});


Breadcrumbs::for('roles.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Grupos', route('admin.roles.index'));
});

Breadcrumbs::for('roles.create', function ($breadcrumb) {
  $breadcrumb->parent('roles.index');
  $breadcrumb->push('Novo Grupo', route('admin.roles.create'));
});

Breadcrumbs::for('roles.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('roles.index');
  $breadcrumb->push('Editar Grupo', route('admin.roles.edit', ['id' => $result->id]));
});


Breadcrumbs::for('permissions.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Permissões', route('admin.permissions.index'));
});

Breadcrumbs::for('permissions.create', function ($breadcrumb) {
  $breadcrumb->parent('permissions.index');
  $breadcrumb->push('Nova Permissão', route('admin.permissions.create'));
});

Breadcrumbs::for('permissions.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('permissions.index');
  $breadcrumb->push('Editar Permissão', route('admin.permissions.edit', ['id' => $result->id]));
});


Breadcrumbs::for('releases.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Destaques', route('admin.releases.index'));
});

Breadcrumbs::for('releases.create', function ($breadcrumb) {
  $breadcrumb->parent('releases.index');
  $breadcrumb->push('Novo Destaque', route('admin.releases.create'));
});

Breadcrumbs::for('releases.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('releases.index');
  $breadcrumb->push('Editar Destaque', route('admin.releases.edit', ['id' => $result->id]));
});


Breadcrumbs::for('actions.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Ações', route('admin.actions.index'));
});

Breadcrumbs::for('actions.create', function ($breadcrumb) {
  $breadcrumb->parent('actions.index');
  $breadcrumb->push('Nova Ação', route('admin.actions.create'));
});

Breadcrumbs::for('actions.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('actions.index');
  $breadcrumb->push('Editar Ação', route('admin.actions.edit', ['id' => $result->id]));
});

Breadcrumbs::for('actions_subs.index', function ($breadcrumb, $action) {
  $breadcrumb->parent('actions.index');
  if (isset($action->slug)) {
    $breadcrumb->push('Subcategorias', route('admin.actions_subs.index', ['slug' => $action->slug]));
  }
});

Breadcrumbs::for('actions_subs.create', function ($breadcrumb, $action) {
  $breadcrumb->parent('actions_subs.index', ['slug' => $action->slug]);
  $breadcrumb->push('Nova Subcategoria', route('admin.actions_subs.create', ['slug' => $action->slug]));
});

Breadcrumbs::for('actions_subs.edit', function ($breadcrumb, $slug, $result) {
  $breadcrumb->parent('actions_subs.index');
  $breadcrumb->push('Editar Subcategoria', route('admin.actions_subs.edit', ['slug' => $result->slug, 'id' => $result->id]));
});


Breadcrumbs::for('actions_posts.index', function ($breadcrumb, $action) {
  $breadcrumb->parent('actions.index');
  if (isset($action->slug)) {
    $breadcrumb->push('Posts', route('admin.actions_posts.index', ['slug' => $action->slug]));
  }
});

Breadcrumbs::for('actions_posts.create', function ($breadcrumb, $action) {
  $breadcrumb->parent('actions_posts.index', ['slug' => $action->slug]);
  $breadcrumb->push('Novo Post', route('admin.actions_posts.create', ['slug' => $action->slug]));
});

Breadcrumbs::for('actions_posts.edit', function ($breadcrumb, $action, $result) {
  $breadcrumb->parent('actions_posts.index', ['slug' => $action->slug]);
  $breadcrumb->push('Editar Post', route('admin.actions_posts.edit', ['slug' => $result->slug, 'id' => $result->id]));
});


Breadcrumbs::for('products.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Produtos', route('admin.products.index'));
});

Breadcrumbs::for('products.create', function ($breadcrumb) {
  $breadcrumb->parent('products.index');
  $breadcrumb->push('Novo Produto', route('admin.products.create'));
});

Breadcrumbs::for('products.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('products.index');
  $breadcrumb->push('Editar Produto', route('admin.products.edit', ['id' => $result->id]));
});

Breadcrumbs::for('products_categories.index', function ($breadcrumb) {
  $breadcrumb->parent('products.index');
  $breadcrumb->push('Categorias', route('admin.products_categories.index'));
});

Breadcrumbs::for('products_categories.create', function ($breadcrumb) {
  $breadcrumb->parent('products_categories.index');
  $breadcrumb->push('Nova Categoria', route('admin.products_categories.create'));
});

Breadcrumbs::for('products_categories.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('products_categories.index');
  $breadcrumb->push('Editar Categoria', route('admin.products_categories.edit', ['id' => $result->id]));
});

Breadcrumbs::for('products_galleries.index', function ($breadcrumb, $product) {
  $breadcrumb->parent('products.index');
  $breadcrumb->push('Galeria', route('admin.products_galleries.index', ['product_id' => $product['product_id']]));
});

Breadcrumbs::for('products_galleries.create', function ($breadcrumb, $product) {
  $breadcrumb->parent('products_galleries.index', ['product_id' => $product->id]);
  $breadcrumb->push('Nova Galeria', route('admin.products_galleries.create', ['product_id' => $product->id]));
});

Breadcrumbs::for('products_galleries.edit', function ($breadcrumb, $result, $product) {
  $breadcrumb->parent('products_galleries.index', ['product_id' => $product->id]);
  $breadcrumb->push('Editar Galeria', route('admin.products_galleries.edit', ['product_id' => $product->id, 'id' => $result->id]));
});


Breadcrumbs::for('projects.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Projetos', route('admin.projects.index'));
});

Breadcrumbs::for('projects.create', function ($breadcrumb) {
  $breadcrumb->parent('projects.index');
  $breadcrumb->push('Novo Projeto', route('admin.projects.create'));
});

Breadcrumbs::for('projects.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('projects.index');
  $breadcrumb->push('Editar Projeto', route('admin.projects.edit', ['id' => $result->id]));
});

Breadcrumbs::for('projects_categories.index', function ($breadcrumb) {
  $breadcrumb->parent('projects.index');
  $breadcrumb->push('Categorias', route('admin.projects_categories.index'));
});

Breadcrumbs::for('projects_categories.create', function ($breadcrumb) {
  $breadcrumb->parent('projects_categories.index');
  $breadcrumb->push('Nova Categoria', route('admin.projects_categories.create'));
});

Breadcrumbs::for('projects_categories.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('projects_categories.index');
  $breadcrumb->push('Editar Categoria', route('admin.projects_categories.edit', ['id' => $result->id]));
});

Breadcrumbs::for('projects_galleries.index', function ($breadcrumb, $project) {
  $breadcrumb->parent('projects.index');
  $breadcrumb->push('Galeria', route('admin.projects_galleries.index', ['project_id' => $project['project_id']]));
});

Breadcrumbs::for('projects_galleries.create', function ($breadcrumb, $project) {
  $breadcrumb->parent('projects_galleries.index', ['project_id' => $project->id]);
  $breadcrumb->push('Nova Galeria', route('admin.projects_galleries.create', ['project_id' => $project->id]));
});

Breadcrumbs::for('projects_galleries.edit', function ($breadcrumb, $result, $project) {
  $breadcrumb->parent('projects_galleries.index', ['project_id' => $project->id]);
  $breadcrumb->push('Editar Galeria', route('admin.projects_galleries.edit', ['project_id' => $project->id, 'id' => $result->id]));
});

Breadcrumbs::for('partners.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Parceiros', route('admin.partners.index'));
});

Breadcrumbs::for('partners.create', function ($breadcrumb) {
  $breadcrumb->parent('partners.index');
  $breadcrumb->push('Novo Parceiro', route('admin.partners.create'));
});

Breadcrumbs::for('partners.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('partners.index');
  $breadcrumb->push('Editar Parceiro', route('admin.partners.edit', ['id' => $result->id]));
});

Breadcrumbs::for('partners_categories.index', function ($breadcrumb) {
  $breadcrumb->parent('partners.index');
  $breadcrumb->push('Categorias', route('admin.partners_categories.index'));
});

Breadcrumbs::for('partners_categories.create', function ($breadcrumb) {
  $breadcrumb->parent('partners_categories.index');
  $breadcrumb->push('Nova Categoria', route('admin.partners_categories.create'));
});

Breadcrumbs::for('partners_categories.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('partners_categories.index');
  $breadcrumb->push('Editar Categoria', route('admin.partners_categories.edit', ['id' => $result->id]));
});

Breadcrumbs::for('posts.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Posts', route('admin.posts.index'));
});

Breadcrumbs::for('posts.create', function ($breadcrumb) {
  $breadcrumb->parent('posts.index');
  $breadcrumb->push('Novo Post', route('admin.posts.create'));
});

Breadcrumbs::for('posts.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('posts.index');
  $breadcrumb->push('Editar Post', route('admin.posts.edit', ['id' => $result->id]));
});

Breadcrumbs::for('posts_categories.index', function ($breadcrumb) {
  $breadcrumb->parent('posts.index');
  $breadcrumb->push('Categorias', route('admin.posts_categories.index'));
});

Breadcrumbs::for('posts_categories.create', function ($breadcrumb) {
  $breadcrumb->parent('posts_categories.index');
  $breadcrumb->push('Nova Categoria', route('admin.posts_categories.create'));
});

Breadcrumbs::for('posts_categories.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('posts_categories.index');
  $breadcrumb->push('Editar Categoria', route('admin.posts_categories.edit', ['id' => $result->id]));
});
