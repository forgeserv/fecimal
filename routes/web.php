<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
Route::name('home.index')->get('/', 'Front\HomeController@index');

// About
Route::name('about.index')->get('/sobre', 'Front\AboutController@index');

// Contact
Route::name('contact.index')->get('/contato', 'Front\ContactController@index');

// News
// Route::name('posts.index')->get('/blog', 'Front\PostController@index');
// Route::name('posts.show')->get('/blog/{category}/{slug}', 'Front\PostController@show');

// Projects
Route::name('projects.index')->get('/projetos/', 'Front\ProjectController@index');
Route::name('projects.show')->get('/projetos/{category}/{slug}', 'Front\ProjectController@show');

// Products
Route::name('products.index')->get('/parceiros', 'Front\ProductController@index');

// Sitemap
Route::name('sitemap')->get('/sitemap', 'Front\SitemapController@index');

/**
 * Api Routes
 */
Route::prefix('api')->group(function()
{
  // Messages
  Route::name('api.message.store')->post('/enviar-mensagem', 'Api\Messages\MessageController@store');

});
