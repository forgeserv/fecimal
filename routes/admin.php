<?php

/**
 * Admin Routes
 */
Route::prefix('admin')->group(function(){

  /**
   * Public Routes
   */
  Route::middleware('guest.admin')->group(function(){
    // Login
    Route::name('admin.session.login')->get('/acessar', 'Admin\Session\SessionController@showLoginForm');
    Route::name('admin.session.login')->post('/acessar', 'Admin\Session\SessionController@login');
    Route::name('autologin')->get('/autologin/{token}', '\Watson\Autologin\AutologinController@autologin');

    // Social Login
    Route::name('admin.social.redirect')->get('/social/redirect/{provider}', 'Admin\Session\SocialController@redirectToProvider');
    Route::name('admin.social.handle')->get('/social/handle/{provider}', 'Admin\Session\SocialController@handleProviderCallback');

    // Reset Password
    Route::name('admin.password.forgot')->get('/esqueci-minha-senha', 'Admin\Session\ForgotPasswordController@showLinkRequestForm');
    Route::name('admin.password.processForgot')->post('/esqueci-minha-senha', 'Admin\Session\ForgotPasswordController@sendResetLinkEmail');
    Route::name('admin.password.reset')->get('/cadastrar-senha/{token}', 'Admin\Session\ResetPasswordController@showResetForm');
    Route::name('admin.password.processReset')->post('/cadastrar-senha', 'Admin\Session\ResetPasswordController@reset');
  });

  /**
   * Protected Routes
   */
  Route::middleware(['auth.admin'])->group(function(){

    // Api Routes
    Route::prefix('api')->group(function(){
      // Media
      Route::prefix('media/images')->group(function(){
        Route::name('admin.media.index')->get('/', 'Api\MediaLibrary\MediaLibraryController@index');
        Route::name('admin.media.store')->post('/adicionar', 'Api\MediaLibrary\MediaLibraryController@store');
      });

      // Places
      Route::name('admin.states.list')->get('/estados', 'Admin\Places\StateController@getStates');
      Route::name('admin.cities.list')->get('/cidades', 'Admin\Places\CityController@getCities');
      Route::name('admin.neighborhoods.list')->get('/bairros', 'Admin\Places\NeighborhoodController@getNeighborhoods');
    });

    // First access
    Route::name('admin.activation.create')->get('/primeiro-acesso', 'Admin\Session\ActivationController@showActivationForm');
    Route::name('admin.activation.processCreate')->post('/primeiro-acesso', 'Admin\Session\ActivationController@store');

    // Logout
    Route::name('admin.session.logout')->get('/sair', 'Admin\Session\SessionController@logout');

    // Check first access
    Route::middleware(['first.access'])->namespace('Admin')->group(function() {

      // Dashboard
      Route::name('admin.dashboard.index')->get('/', 'Dashboard\DashboardController@index');

      // Account
      Route::prefix('perfil')->group(function() {
        Route::name('admin.accounts.show')->get('/', 'Users\AccountController@show');
        Route::name('admin.accounts.edit')->get('/atualizar', 'Users\AccountController@edit');
        Route::name('admin.accounts.update')->put('/atualizar', 'Users\AccountController@update');
      });

       // Products
      Route::prefix('produtos')->group(function() {
        Route::name('admin.products.index')->get('/', 'Products\ProductController@index');
        Route::name('admin.products.create')->get('/adicionar', 'Products\ProductController@create');
        Route::name('admin.products.store')->post('/adicionar', 'Products\ProductController@store');
        Route::name('admin.products.edit')->get('/editar/{id}', 'Products\ProductController@edit');
        Route::name('admin.products.update')->put('/editar/{id}', 'Products\ProductController@update');
        Route::name('admin.products.destroy')->delete('/excluir/{id}', 'Products\ProductController@destroy');

        // Projects Galleries
        Route::prefix('/{product_id}/galeria')->group(function(){
          Route::name('admin.products_galleries.index')->get('/', 'Products\ProductGalleryController@index');
          Route::name('admin.products_galleries.create')->get('/adicionar', 'Products\ProductGalleryController@create');
          Route::name('admin.products_galleries.store')->post('/adicionar', 'Products\ProductGalleryController@store');
          Route::name('admin.products_galleries.edit')->get('/editar/{id}', 'Products\ProductGalleryController@edit');
          Route::name('admin.products_galleries.update')->put('/editar/{id}', 'Products\ProductGalleryController@update');
          Route::name('admin.products_galleries.destroy')->delete('/excluir/{id}', 'Products\ProductGalleryController@destroy');
          Route::name('admin.products_galleries.reorder')->post('/reordenar', 'Products\ProductGalleryController@reorder');
        });
      });

      // Products Categories
      Route::prefix('produtos/categorias')->group(function() {
        Route::name('admin.products_categories.index')->get('/', 'Products\ProductCategoryController@index');
        Route::name('admin.products_categories.create')->get('/adicionar', 'Products\ProductCategoryController@create');
        Route::name('admin.products_categories.store')->post('/adicionar', 'Products\ProductCategoryController@store');
        Route::name('admin.products_categories.edit')->get('/editar/{id}', 'Products\ProductCategoryController@edit');
        Route::name('admin.products_categories.update')->put('/editar/{id}', 'Products\ProductCategoryController@update');
        Route::name('admin.products_categories.destroy')->delete('/excluir/{id}', 'Products\ProductCategoryController@destroy');

      });

      // Projects
      Route::prefix('projetos')->group(function() {
        Route::name('admin.projects.index')->get('/', 'Projects\ProjectController@index');
        Route::name('admin.projects.create')->get('/adicionar', 'Projects\ProjectController@create');
        Route::name('admin.projects.store')->post('/adicionar', 'Projects\ProjectController@store');
        Route::name('admin.projects.edit')->get('/editar/{id}', 'Projects\ProjectController@edit');
        Route::name('admin.projects.update')->put('/editar/{id}', 'Projects\ProjectController@update');
        Route::name('admin.projects.destroy')->delete('/excluir/{id}', 'Projects\ProjectController@destroy');

        // Projects Galleries
        Route::prefix('/{product_id}/galeria')->group(function(){
          Route::name('admin.projects_galleries.index')->get('/', 'Projects\ProjectGalleryController@index');
          Route::name('admin.projects_galleries.create')->get('/adicionar', 'Projects\ProjectGalleryController@create');
          Route::name('admin.projects_galleries.store')->post('/adicionar', 'Projects\ProjectGalleryController@store');
          Route::name('admin.projects_galleries.edit')->get('/editar/{id}', 'Projects\ProjectGalleryController@edit');
          Route::name('admin.projects_galleries.update')->put('/editar/{id}', 'Projects\ProjectGalleryController@update');
          Route::name('admin.projects_galleries.destroy')->delete('/excluir/{id}', 'Projects\ProjectGalleryController@destroy');
          Route::name('admin.projects_galleries.reorder')->post('/reordenar', 'Projects\ProjectGalleryController@reorder');
        });
      });

      // Projects Categories
      Route::prefix('projetos/categorias')->group(function() {
        Route::name('admin.projects_categories.index')->get('/', 'Projects\ProjectCategoryController@index');
        Route::name('admin.projects_categories.create')->get('/adicionar', 'Projects\ProjectCategoryController@create');
        Route::name('admin.projects_categories.store')->post('/adicionar', 'Projects\ProjectCategoryController@store');
        Route::name('admin.projects_categories.edit')->get('/editar/{id}', 'Projects\ProjectCategoryController@edit');
        Route::name('admin.projects_categories.update')->put('/editar/{id}', 'Projects\ProjectCategoryController@update');
        Route::name('admin.projects_categories.destroy')->delete('/excluir/{id}', 'Projects\ProjectCategoryController@destroy');
      });

      // Partners
      Route::prefix('parceiros')->group(function() {
        Route::name('admin.partners.index')->get('/', 'Partners\PartnerController@index');
        Route::name('admin.partners.create')->get('/adicionar', 'Partners\PartnerController@create');
        Route::name('admin.partners.store')->post('/adicionar', 'Partners\PartnerController@store');
        Route::name('admin.partners.edit')->get('/editar/{id}', 'Partners\PartnerController@edit');
        Route::name('admin.partners.update')->put('/editar/{id}', 'Partners\PartnerController@update');
        Route::name('admin.partners.destroy')->delete('/excluir/{id}', 'Partners\PartnerController@destroy');
      });

      // Partners Categories
      Route::prefix('parceiros/categorias')->group(function() {
        Route::name('admin.partners_categories.index')->get('/', 'Partners\PartnerCategoryController@index');
        Route::name('admin.partners_categories.create')->get('/adicionar', 'Partners\PartnerCategoryController@create');
        Route::name('admin.partners_categories.store')->post('/adicionar', 'Partners\PartnerCategoryController@store');
        Route::name('admin.partners_categories.edit')->get('/editar/{id}', 'Partners\PartnerCategoryController@edit');
        Route::name('admin.partners_categories.update')->put('/editar/{id}', 'Partners\PartnerCategoryController@update');
        Route::name('admin.partners_categories.destroy')->delete('/excluir/{id}', 'Partners\PartnerCategoryController@destroy');
      });

      // Posts
      Route::prefix('blog')->group(function() {
        Route::name('admin.posts.index')->get('/', 'Posts\PostController@index');
        Route::name('admin.posts.create')->get('/adicionar', 'Posts\PostController@create');
        Route::name('admin.posts.store')->post('/adicionar', 'Posts\PostController@store');
        Route::name('admin.posts.edit')->get('/editar/{id}', 'Posts\PostController@edit');
        Route::name('admin.posts.update')->put('/editar/{id}', 'Posts\PostController@update');
        Route::name('admin.posts.destroy')->delete('/excluir/{id}', 'Posts\PostController@destroy');
      });

      // Partners Categories
      Route::prefix('blog/categorias')->group(function() {
        Route::name('admin.posts_categories.index')->get('/', 'Posts\PostCategoryController@index');
        Route::name('admin.posts_categories.create')->get('/adicionar', 'Posts\PostCategoryController@create');
        Route::name('admin.posts_categories.store')->post('/adicionar', 'Posts\PostCategoryController@store');
        Route::name('admin.posts_categories.edit')->get('/editar/{id}', 'Posts\PostCategoryController@edit');
        Route::name('admin.posts_categories.update')->put('/editar/{id}', 'Posts\PostCategoryController@update');
        Route::name('admin.posts_categories.destroy')->delete('/excluir/{id}', 'Posts\PostCategoryController@destroy');
      });

      // Permissions
      Route::prefix('permissoes')->group(function() {
        Route::name('admin.permissions.index')->get('/', 'Users\PermissionController@index');
        Route::name('admin.permissions.create')->get('/adicionar', 'Users\PermissionController@create');
        Route::name('admin.permissions.store')->post('/adicionar', 'Users\PermissionController@store');
        Route::name('admin.permissions.edit')->get('/editar/{id}', 'Users\PermissionController@edit');
        Route::name('admin.permissions.update')->put('/editar/{id}', 'Users\PermissionController@update');
        Route::name('admin.permissions.destroy')->delete('/excluir/{id}', 'Users\PermissionController@destroy');
      });

      // Roles
      Route::prefix('grupos')->group(function() {
        Route::name('admin.roles.index')->get('/', 'Users\RoleController@index');
        Route::name('admin.roles.create')->get('/adicionar', 'Users\RoleController@create');
        Route::name('admin.roles.store')->post('/adicionar', 'Users\RoleController@store');
        Route::name('admin.roles.edit')->get('/editar/{id}', 'Users\RoleController@edit');
        Route::name('admin.roles.update')->put('/editar/{id}', 'Users\RoleController@update');
      });

      // Users
      Route::prefix('usuarios')->group(function() {
        Route::name('admin.users.index')->get('/', 'Users\UserController@index');
        Route::name('admin.users.create')->get('/adicionar', 'Users\UserController@create');
        Route::name('admin.users.store')->post('/adicionar', 'Users\UserController@store');
        Route::name('admin.users.edit')->get('/editar/{id}', 'Users\UserController@edit');
        Route::name('admin.users.update')->put('/editar/{id}', 'Users\UserController@update');
        Route::name('admin.users.activate')->get('/ativar/{id}', 'Users\UserController@activate');
        Route::name('admin.users.deactivate')->get('/desativar/{id}', 'Users\UserController@deactivate');
      });

      // Settings
      Route::prefix('configuracoes')->group(function() {
        Route::name('admin.settings.index')->get('/', 'Settings\SettingController@index');
        Route::name('admin.settings.create')->get('/adicionar', 'Settings\SettingController@create');
        Route::name('admin.settings.store')->post('/adicionar', 'Settings\SettingController@store');
        Route::name('admin.settings.edit')->get('/editar/{id}', 'Settings\SettingController@edit');
        Route::name('admin.settings.update')->put('/editar/{id}', 'Settings\SettingController@update');
        Route::name('admin.settings.destroy')->delete('/excluir/{id}', 'Settings\SettingController@destroy');
      });

      // Audit
      Route::prefix('auditoria')->group(function() {
        Route::name('admin.audits.index')->get('/', 'Audits\AuditController@index');
        Route::name('admin.audits.show')->get('/{id}', 'Audits\AuditController@show');
      });

      // Analytics
      Route::name('admin.analytics.index')->get('/acessos', 'Analytics\AnalyticController@index');

      // Messages
      Route::prefix('mensagens')->group(function() {
        Route::name('admin.messages.index')->get('/', 'Messages\MessageController@index');
        Route::name('admin.messages.show')->get('/{id}', 'Messages\MessageController@show');
        Route::name('admin.messages.destroy')->delete('/excluir/{id}', 'Messages\MessageController@destroy');
      });

      // Reports
      Route::prefix('/relatorios')->group(function() {
        Route::name('admin.reports.index')->get('/', 'Reports\ReportController@index');
      });

      // Releases
      Route::prefix('destaques')->group(function() {
        Route::name('admin.releases.index')->get('/', 'Releases\ReleaseController@index');
        Route::name('admin.releases.create')->get('/adicionar', 'Releases\ReleaseController@create');
        Route::name('admin.releases.store')->post('/adicionar', 'Releases\ReleaseController@store');
        Route::name('admin.releases.edit')->get('/editar/{id}', 'Releases\ReleaseController@edit');
        Route::name('admin.releases.update')->put('/editar/{id}', 'Releases\ReleaseController@update');
        Route::name('admin.releases.destroy')->delete('/excluir/{id}', 'Releases\ReleaseController@destroy');
        Route::name('admin.releases.reorder')->post('/reordenar', 'Releases\ReleaseController@reorder');
      });
    });
  });
});
