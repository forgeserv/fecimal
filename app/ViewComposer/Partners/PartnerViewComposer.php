<?php

namespace App\ViewComposer\Partners;

use Illuminate\Contracts\View\View;
use App\Models\Partner\Partner;

class PartnerViewComposer
{
  private $partner;

  public function __construct(Partner $partner)
  {
    // Dependency Injection
    $this->partner = $partner;
  }

  public function compose(View $view)
  {
    // Fetch only
    $factoryGroup = $this->partner->select('id', 'name')->whereCategoryId(1)->actived()->get();

    // Fetch only
    $factoryExcluded = $this->partner->select('id', 'name')->whereCategoryId(2)->actived()->get();

    // Fetch only
    $factoryCertificate = $this->partner->select('id', 'name')->whereCategoryId(3)->actived()->get();

    // View
    $view->with(compact('factoryGroup', 'factoryExcluded', 'factoryCertificate'));
  }

}
