<?php

namespace App\ViewComposer\Products;

use Illuminate\Contracts\View\View;
use App\Models\Product\ProductCategory;

class ProductMenuViewComposer
{
  private $categories;

  public function __construct(ProductCategory $categories)
  {
    // Dependency Injection
    $this->categories = $categories;
  }

  public function compose(View $view)
  {
    // Fetch categories composer views
    $categoriesMenu = $this->categories->select('id', 'slug', 'name')->actived()->get();

    // View
    $view->with(compact('categoriesMenu'));
  }

}
