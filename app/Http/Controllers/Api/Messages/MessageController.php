<?php

namespace App\Http\Controllers\Api\Messages;

use Illuminate\Http\Request;
use App\Models\Auth\User;
use App\Models\Message\Message;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
  private $users;
  private $messages;

  /**
   * Constructor
   */
  public function __construct(User $users, Message $messages)
  {
    // Dependency Injection
    $this->users = $users;
    $this->messages = $messages;
  }

  /**
   * Return view home
   **/
  public function store(Request $request)
  {
    if ($request->wantsJson()):

      $validate = validator($request->all(),[
        'name' => 'required|max:100',
        'email' => 'required|email|max:100',
        'body' => 'required'
      ]);

      // If fails validate
      if($validate->fails()):
        // Return error response
        return response()->json([
          'success' => false,
          'data' => $validate->getMessageBag()
        ], 400);
      endif;

      // Merge
      $request->merge(['subject' => 'Contato atráves da site.']);

      // Create message
      if($message = $this->messages->create($request->all())):
        // Fetch all active and accept receive messages users
        $users = $this->users->select('first_name', 'last_name', 'email')
                              ->where('receive_messages', true)
                              ->where('active', true)
                              ->get();

        // Send the email to users
        foreach($users as $user):
          // Send Message Notification
          // $user->sendContactNotification($message);
        endforeach;

        // Return success response
        return response()->json([
          'success' => true,
          'title'   => 'Obrigado!',
          'message' => 'Sua mensagem foi enviada com sucesso.'
        ], 200);
      endif;
    else:
      abort(404);
    endif;
  }
}
