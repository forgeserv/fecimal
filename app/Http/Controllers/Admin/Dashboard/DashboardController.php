<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Analytics;
use Carbon\Carbon;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Models\Message\Message;
use App\Models\Core\Audit;

class DashboardController extends BaseController
{
  use SEOToolsTrait;

  private $messages;
  private $audits;

  public function __construct(Message $messages, Audit $audits)
  {
    // Dependency Injection
    $this->messages = $messages;
    $this->audits = $audits;
  }

  /**
   * Return index
   **/
  public function index()
  {
    // Only messages
    $messages = $this->messages->select('id', 'name', 'email', 'subject', 'body', 'created_at')->notView()->latest()->limit(3)->get();

    // Get audits today
    $today = Carbon::now();

    // Get events today
    $eventsToday = $this->audits->today()->latest()->limit(4)->get();

    // Set meta tags
    $this->seo()->setTitle('Dashboard');

    // Return view
    return view('admin.dashboard.index', compact('messages', 'today', 'eventsToday'));
  }
}
