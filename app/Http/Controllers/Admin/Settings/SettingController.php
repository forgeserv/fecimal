<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Crumbs;
use Auth;
use App\Models\Core\Setting;

class SettingController extends BaseController
{
  use SEOToolsTrait;

  private $settings;

  /**
   * Constructor
   */
  public function __construct(Setting $settings)
  {
    // Middlewares
    $this->middleware('permission:view_settings', ['only' => ['index']]);
    $this->middleware('permission:add_settings', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_settings', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_settings', ['only' => ['destroy']]);

    // Dependency Injection
    $this->settings = $settings;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request)
  {
    // Fetch all results
    $results = $this->settings->orderBy('title', 'ASC')
                    ->paginate(10);

    // Set breadcrumbs
    Crumbs::addCurrent('Configurações');

    // Set meta tags
    $this->seo()->setTitle('Configurações');

    // Return view
    return view('admin.settings.index', compact('results'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Set breadcrumbs
    Crumbs::add(route('admin.settings.index'), 'Configurações');
    Crumbs::addCurrent('Nova Configuração');

    // Set meta tags
    $this->seo()->setTitle('Nova Configuração');

    // Return view
    return view('admin.settings.create');
  }

  /**
   * Store a newly created resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    $validate = validator($request->all(),[
      'key' => 'required|max:60|unique:settings,key',
      'title' => 'required|max:60',
      'value' => 'required|max:255|unique:settings,value'
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao adicionar configuração.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Create result
    $result = $this->settings->create($request->all());

    // Success message
    flash('Configuração criada com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.settings.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->settings->findOrFail($id);

    // Set breadcrumbs
    Crumbs::add(route('admin.settings.index'), 'Configurações');
    Crumbs::addCurrent('Editar Configuração');

    // Set meta tags
    $this->seo()->setTitle('Editar Configuração');

    // Return view
    return view('admin.settings.edit', compact('result'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $validate = validator($request->all(),[
      'title' => 'required|max:60',
      'value' => "required|max:255|unique:settings,value,{$id}"
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao atualizar configuração.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->settings->findOrFail($id);

    // Fill data and save
    $result->fill($request->except('key'))->save();

    // Success message
    flash('Configuração atualizada com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.settings.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    if ($request->ajax()):

      // Fetch result
      $result = $this->settings->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Configuração removida com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Configuração não encontrada.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover configuração.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
