<?php

namespace App\Http\Controllers\Admin\Projects;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Crumbs;
use Auth;
use App\Models\Project\Project;

class ProjectGalleryController extends BaseController
{
  use SEOToolsTrait;

  private $projects;

  /**
   * Constructor
   */
  public function __construct(Project $projects)
  {
    // Middlewares
    $this->middleware('permission:view_projects', ['only' => ['index']]);
    $this->middleware('permission:add_projects', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_projects', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_projects', ['only' => ['destroy']]);

    // Dependency Injection
    $this->projects = $projects;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request, $project_id)
  {
    // Fetch project
    $project = $this->projects->select('id', 'title')->findOrFail($project_id);

    // Fetch all results
    $results = $project->media()->where('collection_name', 'projects_galleries')
                             ->orderBy('order_column', 'ASC')
                             ->paginate(6);

    // Set meta tags
    $this->seo()->setTitle("Galeria - {$project->title}");

    // Return view
    return view('admin.projects_galleries.index', compact('project', 'results'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create($project_id)
  {
    // Fetch projects
    $project = $this->projects->select('id', 'title')->findOrFail($project_id);

    // Set meta tags
    $this->seo()->setTitle('Adicionar Imagens');

    // Return view
    return view('admin.projects_galleries.create', compact('project'));
  }

  /**
   * Store a newly created resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function store(Request $request, $project_id)
  {
    $validate = validator($request->all(),[
      'file' => 'image|max:4000'
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      $message = $validate->getMessageBag()->first('file', ':message');
      $status = 400;
    endif;

    // Fetch result
    $result = $this->projects->select('id', 'title')->findOrFail($project_id);

    if($result):

      // Request image
      $image = $request->file('file');

      // Upload image if send
      if ($image):
        $filename = md5($image->getClientOriginalName()) . '.' . $image->getClientOriginalExtension();
        $result->addMedia($image)
                ->usingName($result->title)
                ->usingFileName($filename)
                ->toMediaCollection('projects_galleries');
      endif;


      // Success message
      $message = 'Imagem adicionada com sucesso.';
      $status = 200;

    else:

      // Warning message
      $message = 'Não foi possível enviar a imagem.';
      $status = 400;

    endif;

    // Return response
    return response()->json($message, $status);
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($project_id, $id)
  {
    // Fetch project
    $project = $this->projects->select('id', 'title')->findOrFail($project_id);

    // Fetch result by id
    $result = $project->media()->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle('Editar Imagem');

    // Return view
    return view('admin.projects_galleries.edit', compact('project', 'result'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $project_id, $id)
  {
    $validate = validator($request->all(),[
      'name' => 'required|max:255'
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao atualizar imagem.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch project
    $project = $this->projects->select('id', 'title')->findOrFail($project_id);

    // Fetch result
    $result = $project->media()->findOrFail($id);

    // Fill data and save
    $result->fill($request->all())->save();

    // Success message
    flash('Imagem atualizada com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.projects_galleries.index', ['project_id' => $project->id]);
  }

  /**
   * Reorder the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function reorder(Request $request, $project_id)
  {
    if ($request->ajax()):

      // Fetch project
      $project = $this->projects->select('id', 'title')->findOrFail($project_id);

      if($project):

        // Get list
        $list = $request->get('list');

        // Set new order
        $startOrder = 1;

        foreach ($list as $key => $item):
          $result = $project->media()->find($item);
          $result->order_column = $startOrder++;
          $result->save();
        endforeach;

        // Return success response
        return response()->json(['success' => true, 'message' => 'Galeria reordenada com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Falha ao reordenar galeria.'], 400);

      endif;

    else:

      abort(404);

    endif;
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $project_id, $id)
  {
    if ($request->ajax()):

      // Fetch project
      $project = $this->projects->select('id', 'title')->findOrFail($project_id);

      // If project exist
      if($project):

        // Find result by id
        $result = $project->media()->find($id);

        // If result exist
        if($result):

          // Remove result
          $result->delete();

          // Return success response
          return response()->json(['success' => true, 'message' => 'Imagem removida com sucesso.'], 200);

        else:

          // Return error response
          return response()->json(['success' => false, 'message' => 'Imagem não encontrada.'], 400);

        endif;

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Produto não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover imagem.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
