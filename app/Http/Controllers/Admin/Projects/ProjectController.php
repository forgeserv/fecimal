<?php

namespace App\Http\Controllers\Admin\Projects;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Project\Project;
use App\Models\Project\ProjectCategory;

class ProjectController extends BaseController
{
  use SEOToolsTrait;

  private $projects;
  private $categories;

  /**
   * Constructor
   */
  public function __construct(Project $projects, ProjectCategory $categories)
  {
    // Middlewares
    $this->middleware('permission:view_projects', ['only' => ['index']]);
    $this->middleware('permission:add_projects', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_projects', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_projects', ['only' => ['destroy']]);

    // Dependency Injection
    $this->projects = $projects;
    $this->categories = $categories;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->projects->select('id', 'title', 'slug', 'publish', 'category_id')->orderBy('id', 'DESC');

    // Filter by nome param
    if ($request->filled('nome')) {
      $name = $request->get('nome');
      $query->where('title', 'like', "%{$name}%");
    }

    // Filter by category param
    if ($request->filled('categoria')) {
      $categoria = $request->get('categoria');
      $query->whereHas('category', function ($q) use ($categoria) {
        $q->whereSlug($categoria);
      });
    }

    // Fetch categories filter
    $categories = $this->categories->select('id', 'slug', 'name')->actived()->pluck('name', 'slug');

    // Fetch all results
    $results = $query->paginate(5)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Projetos');

    // Return view
    return view('admin.projects.index', compact('results', 'categories'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Fetch categories
    $categories = $this->categories->select('id', 'name')->actived()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Novo Projeto');

    // Return view
    return view('admin.projects.create', compact('categories'));
  }

  /**
   * Store a newly created resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    $validate = validator($request->all(),[
      'title' => 'required|max:255',
      'subtitle' => 'required|max:255',
      'legend' => 'nullable|max:255',
      'owner' => 'nullable|max:255',
      'architect' => 'nullable|max:255',
      'body' => 'required',
      'year' => 'required|max:255',
      'category_id' => 'required',
      'cover' => 'required|image|max:4000',
      'header' => 'required|image|max:4000',
      'seo.meta_title' => 'required|max:255',
      'seo.meta_description' => 'required|max:255',
      'seo.image_facebook' => 'nullable|image|max:2000'
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao adicionar Projeto.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Create result
    $result = $this->projects->create($request->except('seo'));

    // Request image desktop
    $cover = $request->file('cover');

    // Upload cover if send
    if ($cover):
      $filename = md5($cover->getClientOriginalName()) . '.' . $cover->getClientOriginalExtension();
      $result->addMedia($cover)
              ->usingName($result->title)
              ->usingFileName($filename)
              ->toMediaCollection('project');
    endif;

    // Request image desktop
    $header = $request->file('header');

    // Upload cover if send
    if ($header):
      $filename = md5($header->getClientOriginalName()) . '.' . $header->getClientOriginalExtension();
      $result->addMedia($header)
              ->usingName($result->title)
              ->usingFileName($filename)
              ->toMediaCollection('project_header');
    endif;

     // Create seo tags
    $seo = $result->seo()->create($request->get('seo'));

    // Request file
    $image_facebook = $request->file('image_facebook');

    // Upload image_facebook if send
    if ($image_facebook):
      $filename = md5($image_facebook->getClientOriginalName()) . '.' . $image_facebook->getClientOriginalExtension();
      $seo->addMedia($image_facebook)
          ->usingName($result->title)
          ->usingFileName($filename)
          ->toMediaCollection('facebook_share');
    endif;

    // Success message
    flash('Projeto criado com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.projects.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->projects->findOrFail($id);

    // Fetch categories
    $categories = $this->categories->select('id', 'name')->actived()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Editar Projeto');

    // Return view
    return view('admin.projects.edit', compact('result', 'categories'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $validate = validator($request->all(),[
      'title' => 'required|max:255',
      'subtitle' => 'required|max:255',
      'legend' => 'nullable|max:255',
      'owner' => 'nullable|max:255',
      'architect' => 'nullable|max:255',
      'body' => 'required',
      'year' => 'required|max:255',
      'category_id' => 'required',
      'cover' => 'nullable|image|max:4000',
      'header' => 'nullable|image|max:4000',
      'seo.meta_title' => 'required|max:255',
      'seo.meta_description' => 'required|max:255',
      'seo.image_facebook' => 'nullable|image|max:2000'
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao atualizar Projeto.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->projects->findOrFail($id);

    // Fill data and save
    $result->fill($request->all())->save();

    // Request image desktop
    $cover = $request->file('cover');

    // Upload cover if send
    if ($cover):
      $result->clearMediaCollection('project');
      $filename = md5($cover->getClientOriginalName()) . '.' . $cover->getClientOriginalExtension();
      $result->addMedia($cover)
              ->usingName($result->title)
              ->usingFileName($filename)
              ->toMediaCollection('project');
    endif;

    // Request image desktop
    $header = $request->file('header');

    // Upload header if send
    if ($header):
      $result->clearMediaCollection('project_header');
      $filename = md5($header->getClientOriginalName()) . '.' . $header->getClientOriginalExtension();
      $result->addMedia($header)
              ->usingName($result->title)
              ->usingFileName($filename)
              ->toMediaCollection('project_header');
    endif;

    // Create or update seo tags
    $seo = $result->seo()->updateOrCreate(['id' => $request->input('seo.id')], $request->get('seo'));

    // Request image desktop
    $image_facebook = $request->file('image_facebook');

    // Upload image_facebook if send
    if ($image_facebook):
      $seo->clearMediaCollection('facebook_share');
      $filename = md5($image_facebook->getClientOriginalName()) . '.' . $image_facebook->getClientOriginalExtension();
      $seo->addMedia($image_facebook)
          ->usingName($result->title)
          ->usingFileName($filename)
          ->toMediaCollection('facebook_share');
    endif;

    // Success message
    flash('Projeto atualizado com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.projects.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    if ($request->ajax()):

      // Fetch result
      $result = $this->projects->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Projeto removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Projeto não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Produto.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
