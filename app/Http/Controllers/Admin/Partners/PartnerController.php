<?php

namespace App\Http\Controllers\Admin\Partners;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Partner\Partner;
use App\Models\Partner\PartnerCategory;

class PartnerController extends BaseController
{
  use SEOToolsTrait;

  private $partners;
  private $categories;

  /**
   * Constructor
   */
  public function __construct(Partner $partners, PartnerCategory $categories)
  {
    // Middlewares
    $this->middleware('permission:view_partners', ['only' => ['index']]);
    $this->middleware('permission:add_partners', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_partners', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_partners', ['only' => ['destroy']]);

    // Dependency Injection
    $this->partners = $partners;
    $this->categories = $categories;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->partners->select('id', 'name', 'active', 'category_id')->orderBy('id', 'DESC');

    // Filter by nome param
    if ($request->filled('nome')) {
      $name = $request->get('nome');
      $query->where('name', 'like', "%{$name}%");
    }

    // Filter by category param
    if ($request->filled('categoria')) {
      $categoria = $request->get('categoria');
      $query->whereHas('category', function ($q) use ($categoria) {
        $q->whereSlug($categoria);
      });
    }

    // Fetch categories filter
    $categories = $this->categories->select('id', 'slug', 'name')->actived()->pluck('name', 'slug');

    // Fetch all results
    $results = $query->paginate(5)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Parceiros');

    // Return view
    return view('admin.partners.index', compact('results', 'categories'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Fetch categories
    $categories = $this->categories->select('id', 'name')->actived()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Novo Parceiro');

    // Return view
    return view('admin.partners.create', compact('categories'));
  }

  /**
   * Store a newly created resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    $validate = validator($request->all(),[
      'name' => 'required|max:255|unique:partners',
      'refer' => 'required|max:255',
      'category_id' => 'required',
      'cover' => 'required|image|max:4000',
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao adicionar Parceiro.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Create result
    $result = $this->partners->create($request->all());

    // Request image desktop
    $cover = $request->file('cover');

    // Upload cover if send
    if ($cover):
      $filename = md5($cover->getClientOriginalName()) . '.' . $cover->getClientOriginalExtension();
      $result->addMedia($cover)
              ->usingName($result->name)
              ->usingFileName($filename)
              ->toMediaCollection('partner');
    endif;

    // Success message
    flash('Parceiro criado com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.partners.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->partners->findOrFail($id);

    // Fetch categories
    $categories = $this->categories->select('id', 'name')->actived()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Editar Parceiro');

    // Return view
    return view('admin.partners.edit', compact('result', 'categories'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $validate = validator($request->all(),[
      'name' => 'required|max:255|unique:partners,name,' . $id,
      'refer' => 'required|max:255',
      'category_id' => 'required',
      'cover' => 'nullable|image|max:4000',
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao atualizar Parceiro.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->partners->findOrFail($id);

    // Fill data and save
    $result->fill($request->all())->save();

    // Request image desktop
    $cover = $request->file('cover');

    // Upload cover if send
    if ($cover):
      $result->clearMediaCollection('partner');
      $filename = md5($cover->getClientOriginalName()) . '.' . $cover->getClientOriginalExtension();
      $result->addMedia($cover)
              ->usingName($result->name)
              ->usingFileName($filename)
              ->toMediaCollection('partner');
    endif;

    // Success message
    flash('Parceiro atualizado com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.partners.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    if ($request->ajax()):

      // Fetch result
      $result = $this->partners->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Parceiro removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Parceiro não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Parceiro.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
