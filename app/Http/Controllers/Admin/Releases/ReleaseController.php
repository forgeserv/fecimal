<?php

namespace App\Http\Controllers\Admin\Releases;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Crumbs;
use Auth;
use App\Models\Release\Release;

class ReleaseController extends BaseController
{
  use SEOToolsTrait;

  private $releases;

  /**
   * Constructor
   */
  public function __construct(Release $releases)
  {
    // Middlewares
    $this->middleware('permission:view_releases', ['only' => ['index']]);
    $this->middleware('permission:add_releases', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_releases', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_releases', ['only' => ['destroy']]);

    // Dependency Injection
    $this->releases = $releases;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request)
  {
    // Fetch all results
    $results = $this->releases->orderBy('position', 'ASC')->get();

    // Set meta tags
    $this->seo()->setTitle('Destaques');

    // Return view
    return view('admin.releases.index', compact('results'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Set meta tags
    $this->seo()->setTitle('Novo Destaque');

    // Return view
    return view('admin.releases.create');
  }

  /**
   * Store a newly created resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    $validate = validator($request->all(),[
      'title' => 'required|max:255',
      'subtitle' => 'required|max:255',
      'button' => 'nullable|required_with:refer|max:100',
      'refer' => 'nullable|required_with:button|url|max:255',
      'target' => 'nullable|required_with:button',
      'image_desktop' => 'required|image|max:5000',
      'image_mobile' => 'required|image|max:5000'
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao adicionar destaque.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Create result
    $result = $this->releases->create($request->except('image_desktop', 'image_mobile'));

    // Request image desktop
    $image_desktop = $request->file('image_desktop');

    // Upload image_desktop if send
    if ($image_desktop):
      $filename = md5($image_desktop->getClientOriginalName()) . '.' . $image_desktop->getClientOriginalExtension();
      $result->addMedia($image_desktop)
              ->usingName($result->title)
              ->usingFileName($filename)
              ->toMediaCollection('release_desktop');
    endif;

    // Request image mobile
    $image_mobile = $request->file('image_mobile');

    // Upload image_mobile if send
    if ($image_mobile):
      $filename = md5($image_mobile->getClientOriginalName()) . '.' . $image_mobile->getClientOriginalExtension();
      $result->addMedia($image_mobile)
              ->usingName($result->title)
              ->usingFileName($filename)
              ->toMediaCollection('release_mobile');
    endif;

    // Success message
    flash('Destaque criado com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.releases.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->releases->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle('Editar Destaque');

    // Return view
    return view('admin.releases.edit', compact('result'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $validate = validator($request->all(),[
      'title' => 'required|max:255',
      'subtitle' => 'required|max:255',
      'button' => 'nullable|required_with:refer|max:100',
      'refer' => 'nullable|required_with:button|url|max:255',
      'target' => 'nullable|required_with:button',
      'image_desktop' => 'image|max:5000',
      'image_mobile' => 'image|max:5000'
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao atualizar destaque.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->releases->findOrFail($id);

    // Fill data and save
    $result->fill($request->except('image_desktop', 'image_mobile'))->save();

    // Request images
    $image_desktop = $request->file('image_desktop');

    // Upload image_desktop if send
    if ($image_desktop):
      $result->clearMediaCollection('release_desktop');
      $filename = md5($image_desktop->getClientOriginalName()) . '.' . $image_desktop->getClientOriginalExtension();
      $result->addMedia($image_desktop)
              ->usingName($result->title)
              ->usingFileName($filename)
              ->toMediaCollection('release_desktop');
    endif;

    // Request image mobile
    $image_mobile = $request->file('image_mobile');

    // Upload image_mobile if send
    if ($image_mobile):
      $result->clearMediaCollection('release_mobile');
      $filename = md5($image_mobile->getClientOriginalName()) . '.' . $image_mobile->getClientOriginalExtension();
      $result->addMedia($image_mobile)
              ->usingName($result->title)
              ->usingFileName($filename)
              ->toMediaCollection('release_mobile');
    endif;

    // Success message
    flash('Destaque atualizado com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.releases.index');
  }

  /**
   * Reorder the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function reorder(Request $request)
  {
    if ($request->ajax()):

      // Get list
      $list = $request->get('list');

      // Set new order
      $startOrder = 1;

      foreach ($list as $key => $item):
        $release = $this->releases->find($item);
        $release->position = $startOrder++;
        $release->save();
      endforeach;

      // Return success response
      return response()->json(['success' => true, 'message' => 'Destaques reordenados com sucesso.'], 200);

    else:

      abort(404);

    endif;
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    if ($request->ajax()):

      // Fetch result
      $result = $this->releases->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Destaque removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Destaque não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover destaque.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
