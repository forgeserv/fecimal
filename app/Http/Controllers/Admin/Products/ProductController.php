<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Crumbs;
use Auth;
use App\Models\Product\Product;
use App\Models\Product\ProductCategory;

class ProductController extends BaseController
{
  use SEOToolsTrait;

  private $products;
  private $categories;

  /**
   * Constructor
   */
  public function __construct(Product $products, ProductCategory $categories)
  {
    // Middlewares
    $this->middleware('permission:view_products', ['only' => ['index']]);
    $this->middleware('permission:add_products', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_products', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_products', ['only' => ['destroy']]);

    // Dependency Injection
    $this->products = $products;
    $this->categories = $categories;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->products->select('id', 'name', 'slug', 'publish', 'category_id')->orderBy('name', 'ASC');

    // Filter by nome param
    if ($request->filled('nome')) {
      $name = $request->get('nome');
      $query->where('name', 'like', "%{$name}%");
    }

    // Filter by category param
    if ($request->filled('categoria')) {
      $categoria = $request->get('categoria');
      $query->whereHas('category', function ($q) use ($categoria) {
        $q->whereSlug($categoria);
      });
    }

    // Fetch categories filter
    $categories = $this->categories->select('id', 'slug', 'name')->actived()->pluck('name', 'slug');

    // Fetch all results
    $results = $query->paginate(5)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Produtos');

    // Return view
    return view('admin.products.index', compact('results', 'categories'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Fetch categories
    $categories = $this->categories->select('id', 'name')->actived()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Novo Produto');

    // Return view
    return view('admin.products.create', compact('categories'));
  }

  /**
   * Store a newly created resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    $validate = validator($request->all(),[
      'name' => 'required|max:255|unique:products',
      'category_id' => 'required',
      'cover' => 'required|image|max:4000',
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao adicionar Produto.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Create result
    $result = $this->products->create($request->all());

    // Request image desktop
    $cover = $request->file('cover');

    // Upload cover if send
    if ($cover):
      $filename = md5($cover->getClientOriginalName()) . '.' . $cover->getClientOriginalExtension();
      $result->addMedia($cover)
            ->usingName($result->name)
            ->usingFileName($filename)
            ->toMediaCollection('product');
    endif;

    // Success message
    flash('Produto criado com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.products.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->products->findOrFail($id);

    // Fetch categories
    $categories = $this->categories->select('id', 'name')->actived()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Editar Produto');

    // Return view
    return view('admin.products.edit', compact('result', 'categories'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $validate = validator($request->all(),[
      'name' => 'required|max:255|unique:products,name, ' . $id,
      'category_id' => 'required',
      'cover' => 'nullable|image|max:4000',
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao atualizar Produto.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->products->findOrFail($id);

    // Fill data and save
    $result->fill($request->all())->save();

    // Request image desktop
    $cover = $request->file('cover');

    // Upload cover if send
    if ($cover):
      $result->clearMediaCollection('product');
      $filename = md5($cover->getClientOriginalName()) . '.' . $cover->getClientOriginalExtension();
      $result->addMedia($cover)
            ->usingName($result->name)
            ->usingFileName($filename)
            ->toMediaCollection('product');
    endif;

    // Success message
    flash('Produto atualizada com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.products.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    if ($request->ajax()):

      // Fetch result
      $result = $this->products->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Produto removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Produto não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Produto.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
