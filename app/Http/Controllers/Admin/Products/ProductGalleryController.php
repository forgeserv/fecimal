<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Crumbs;
use Auth;
use App\Models\Product\Product;

class ProductGalleryController extends BaseController
{
  use SEOToolsTrait;

  private $products;

  /**
   * Constructor
   */
  public function __construct(Product $products)
  {
    // Middlewares
    $this->middleware('permission:view_products', ['only' => ['index']]);
    $this->middleware('permission:add_products', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_products', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_products', ['only' => ['destroy']]);

    // Dependency Injection
    $this->products = $products;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request, $product_id)
  {
    // Fetch product
    $product = $this->products->select('id', 'name')->findOrFail($product_id);

    // Fetch all results
    $results = $product->media()->where('collection_name', 'products_galleries')
                             ->orderBy('order_column', 'ASC')
                             ->get();

    // Set meta tags
    $this->seo()->setTitle("Produtos - Galerias - {$product->name}");

    // Return view
    return view('admin.products_galleries.index', compact('product', 'results'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create($product_id)
  {
    // Fetch products
    $product = $this->products->select('id', 'name')->findOrFail($product_id);

    // Set meta tags
    $this->seo()->setTitle('Produtos - Galerias - Adicionar Galeria');

    // Return view
    return view('admin.products_galleries.create', compact('product'));
  }

  /**
   * Store a newly created resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function store(Request $request, $product_id)
  {
    $validate = validator($request->all(),[
      'file' => 'image|max:6000'
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      $message = $validate->getMessageBag()->first('file', ':message');
      $status = 400;
    endif;

    // Fetch result
    $result = $this->products->select('id', 'name')->findOrFail($product_id);

    if($result):

      // Request image
      $image = $request->file('file');

      // Upload image if send
      if ($image):
        $filename = md5($image->getClientOriginalName()) . '.' . $image->getClientOriginalExtension();
        $result->addMedia($image)
                ->usingName($result->name)
                ->usingFileName($filename)
                ->toMediaCollection('products_galleries');
      endif;


      // Success message
      $message = 'Imagem adicionada com sucesso.';
      $status = 200;

    else:

      // Warning message
      $message = 'Não foi possível enviar a imagem.';
      $status = 400;

    endif;

    // Return response
    return response()->json($message, $status);
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($product_id, $id)
  {
    // Fetch product
    $product = $this->products->select('id', 'name')->findOrFail($product_id);

    // Fetch result by id
    $result = $product->media()->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle('Produtos - Galerias - Editar Galeria');

    // Return view
    return view('admin.products_galleries.edit', compact('product', 'result'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $product_id, $id)
  {
    $validate = validator($request->all(),[
      'name' => 'required|max:255'
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao atualizar Imagem.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch product
    $product = $this->products->select('id', 'name')->findOrFail($product_id);

    // Fetch result
    $result = $product->media()->findOrFail($id);

    // Fill data and save
    $result->fill($request->all())->save();

    // Success message
    flash('Imagem atualizada com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.products_galleries.index', ['product_id' => $product->id]);
  }

  /**
   * Reorder the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function reorder(Request $request, $product_id)
  {
    if ($request->ajax()):

      // Fetch product
      $product = $this->products->select('id', 'name')->findOrFail($product_id);

      if($product):

        // Get list
        $list = $request->get('list');

        // Set new order
        $startOrder = 1;

        foreach ($list as $key => $item):
          $result = $product->media()->find($item);
          $result->order_column = $startOrder++;
          $result->save();
        endforeach;

        // Return success response
        return response()->json(['success' => true, 'message' => 'Galeria reordenada com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Falha ao reordenar galeria.'], 400);

      endif;

    else:

      abort(404);

    endif;
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $product_id, $id)
  {
    if ($request->ajax()):

      // Fetch product
      $product = $this->products->select('id', 'name')->findOrFail($product_id);

      // If product exist
      if($product):

        // Find result by id
        $result = $product->media()->find($id);

        // If result exist
        if($result):

          // Remove result
          $result->delete();

          // Return success response
          return response()->json(['success' => true, 'message' => 'Imagem removida com sucesso.'], 200);

        else:

          // Return error response
          return response()->json(['success' => false, 'message' => 'Imagem não encontrada.'], 400);

        endif;

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Produto não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover imagem.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
