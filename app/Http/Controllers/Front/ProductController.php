<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Product\Product;
use App\Models\Product\ProductCategory;
use App\Models\Partner\Partner;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
  use SEOToolsTrait;

  protected $products;
  protected $categories;
  protected $partners;

  public function __construct(Partner $partners, Product $products, ProductCategory $categories)
  {
    // Dependency Injection
    $this->products = $products;
    $this->categories = $categories;
    $this->partners = $partners;
  }

  public function index($slug, $product = null)
  {
    // // Find category
    // $category = $this->categories->select('id', 'slug', 'name')->whereSlug($slug)->actived()->firstOrFail();

    // // Init filter
    // $productFilter = null;

    // // Find product
    // if (!is_null($product)) {
    //   $productFilter = $this->products->whereSlug($product)->firstOrFail();
    // } else {
    //   $productFilter = $category->products->first();
    // }

    // Fetch partners
    $partners = $this->partners->where('active', true)->get();

    // Seo
    $this->seo()->setTitle('Parceiros');

    // Set meta tags
    $this->seo()->setDescription('A Fecimal arte e madeira  atua na fabricação de fábrica de esquadrias , Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.')
                ->metatags();

    // Set opengraph tags
    $this->seo()->opengraph()
                ->setDescription('A Fecimal arte e madeira  atua na fabricação de fábrica de esquadrias , Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.')
                ->addImage();

    // Set twitter tags
    $this->seo()->twitter()
                ->setDescription('A Fecimal arte e madeira  atua na fabricação de fábrica de esquadrias , Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.');

    // Return view
    return view('front.products.index', compact('partners'));
  }
}
