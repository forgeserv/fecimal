<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Project\Project;
use App\Models\Project\ProjectCategory;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
  use SEOToolsTrait;

  protected $projects;
  protected $categories;

  public function __construct(Project $projects, ProjectCategory $categories)
  {
    // Dependency Injection
    $this->projects = $projects;
    $this->categories = $categories;
  }

  public function index()
  {
    // Get all projects
    $projects = $this->projects->select('id', 'title', 'slug', 'category_id', 'owner', 'architect')->published()->get();

    // Seo
    $this->seo()->setTitle('Projetos');

    // Set meta tags
    $this->seo()->setDescription('A Fecimal arte e madeira  atua na fabricação de fábrica de esquadrias , Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.')
                ->metatags();

    // Set opengraph tags
    $this->seo()->opengraph()
                ->setDescription('A Fecimal arte e madeira  atua na fabricação de fábrica de esquadrias , Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.')
                ->addImage();

    // Set twitter tags
    $this->seo()->twitter()
                ->setDescription('A Fecimal arte e madeira  atua na fabricação de fábrica de esquadrias , Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.');

    // Return view
    return view('front.projects.index', compact('projects'));
  }

    /**
  * Return page show
  **/
  public function show($category, $slug)
  {
    // Fetch category by slug
    $category = $this->categories->select('id', 'name', 'slug')->where('slug', $category)->firstOrFail();

    // Fetch page by slug
    $project = $this->projects->where('slug', $slug)
                        ->where('category_id', $category->id)
                        ->firstOrFail();
    // Incremment page
    $project->increment('views');

    // Gallerys
    $gallery = $project->gallery;

    $galleryFirstCol = [];

    if($gallery->count() > 0) {
      foreach ($gallery as $key => $photo) {
        if ($key <= 2) {
          $galleryFirstCol['imagens'][$key] = $photo;
        }
      }
    }

    $gallerySecondCol = $gallery->filter(function ($value, $key) {
      return $key > 2;
    });

    // Set meta tags
    $this->seo()->setTitle("{$project->seo->meta_title} | Projetos")
                ->setDescription("{$project->seo->meta_description} | Projetos");

    // Set opengraph tags
    $this->seo()->opengraph()->setTitle("{$project->seo->og_title} | Projetos")
                ->setDescription("{$project->seo->og_description} | Projetos")
                ->addImage($project->seo->facebook_image);

    // Return view
    return view('front.projects.show', compact('category', 'project', 'galleryFirstCol', 'gallerySecondCol'));
  }
}
