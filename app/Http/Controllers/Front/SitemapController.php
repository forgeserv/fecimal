<?php

namespace App\Http\Controllers\Front;

use Carbon\Carbon;
use App\Models\Project\Project;
use App\Models\Product\Product;
use App\Http\Controllers\Controller;

class SitemapController extends Controller
{
  private $projects;
  private $products;

  public function __construct(Project $projects, Product $products)
  {
    // Dependency Injection
    $this->projects = $projects;
    $this->products = $products;
  }

  /**
   * Return view home
   **/
  public function index()
  {
    // Create new sitemap object
    $sitemap = app()->make('sitemap');
    \Cache::flush();

    // Check if there is cached sitemap and build new only if is not
    if (!$sitemap->isCached()):

      // Add home to sitemap
      $sitemap->add(route('home.index'), '2018-08-16T20:10:00+02:00', '1.0', 'daily');

      // Add about to sitemap
      $sitemap->add(route('about.index'), '2018-08-16T20:10:00+02:00', '0.9', 'weekly');

      // Add contact to sitemap
      $sitemap->add(route('contact.index'), '2018-08-16T20:10:00+02:00', '0.9', 'weekly');

      // Add posts to sitemap
      $sitemap->add(route('posts.index'), '2018-08-16T20:10:00+02:00', '0.9', 'weekly');

      // Add projects to sitemap
      $sitemap->add(route('projects.index'), '2018-08-16T20:10:00+02:00', '0.9', 'weekly');

      // Projects
      $projects = $this->projects->select('id', 'slug', 'title', 'category_id', 'created_at', 'updated_at')->published()->get();

      // Add every project to the sitemap
      foreach ($projects as $project):
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $project->updated_at)->toAtomString();
        $sitemap->add(route('projects.show', ['category' => $project->category->slug, 'slug' => $project->slug]), '2018-05-16T20:10:00+02:00', '0.7', 'monthly');
      endforeach;

    endif;

    // Return sitemap
    return $sitemap->render('xml');
  }
}
