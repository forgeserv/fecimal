<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Release\Release;
use App\Models\Project\Project;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
  use SEOToolsTrait;

  protected $releases;
  protected $projects;

  public function __construct(Release $releases, Project $projects)
  {
    // Dependency Injection
    $this->releases = $releases;
    $this->projects = $projects;
  }

  public function index()
  {
    // Fetch Releases
    $releases = $this->releases->select('id', 'title', 'subtitle', 'button', 'refer', 'target', 'active')->actived()->get();

    // Fetch Projects
    $projects = $this->projects->select('id', 'slug', 'title', 'subtitle', 'category_id', 'body', 'link_extern')->published()->releases()->get();

    // Set meta tags
    $this->seo()->setDescription('A Fecimal arte e madeira  atua na fabricação de fábrica de esquadrias , Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.')
                ->metatags();

    // Set opengraph tags
    $this->seo()->opengraph()
                ->setDescription('A Fecimal arte e madeira  atua na fabricação de fábrica de esquadrias , Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.')
                ->addImage();

    // Set twitter tags
    $this->seo()->twitter()
                ->setDescription('A Fecimal arte e madeira  atua na fabricação de fábrica de esquadrias , Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.');

    // Return view
    return view('front.home.index', compact('releases', 'projects'));
  }
}
