<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Post\Post;
use App\Models\Post\PostCategory;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
  use SEOToolsTrait;

  protected $posts;
  protected $categories;

  public function __construct(Post $posts, PostCategory $categories)
  {
    // Dependency Injection
    $this->posts = $posts;
    $this->categories = $categories;
  }

  public function index()
  {
    // Get all posts
    $posts = $this->posts->select('id', 'title', 'subtitle', 'slug', 'category_id')->published()->get();

    // Seo
    $this->seo()->setTitle('Blog');

    // Set meta tags
    $this->seo()->setDescription('A Fecimal arte e madeira  atua na fabricação de fábrica de esquadrias , Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.')
                ->metatags();

    // Set opengraph tags
    $this->seo()->opengraph()
                ->setDescription('A Fecimal arte e madeira  atua na fabricação de fábrica de esquadrias , Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.')
                ->addImage();

    // Set twitter tags
    $this->seo()->twitter()
                ->setDescription('A Fecimal arte e madeira  atua na fabricação de fábrica de esquadrias , Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.');

    // Return view
    return view('front.posts.index', compact('posts'));
  }

    /**
  * Return page show
  **/
  public function show($category, $slug)
  {
    // Fetch category by slug
    $category = $this->categories->select('id', 'name', 'slug')->where('slug', $category)->firstOrFail();

    // Fetch page by slug
    $post = $this->posts->where('slug', $slug)->where('category_id', $category->id)->firstOrFail();

    // Incremment page
    $post->increment('views');

    // Set meta tags
    $this->seo()->setTitle("{$post->seo->meta_title} | Blog")
                ->setDescription("{$post->seo->meta_description} | Blog");

    // Set opengraph tags
    $this->seo()->opengraph()->setTitle("{$post->seo->og_title} | Blog")
                ->setDescription("{$post->seo->og_description} | Blog")
                ->addImage($post->seo->facebook_image);

    // Return view
    return view('front.posts.show', compact('category', 'post'));
  }
}
