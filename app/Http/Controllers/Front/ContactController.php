<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
  use SEOToolsTrait;

  public function index()
  {
    // Seo
    $this->seo()->setTitle('Contato');

   // Set meta tags
    $this->seo()->setDescription('A Fecimal arte e madeira  atua na fabricação de fábrica de esquadrias , Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.')
                ->metatags();

    // Set opengraph tags
    $this->seo()->opengraph()
                ->setDescription('A Fecimal arte e madeira  atua na fabricação de fábrica de esquadrias , Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.')
                ->addImage();

    // Set twitter tags
    $this->seo()->twitter()
                ->setDescription('A Fecimal arte e madeira  atua na fabricação de fábrica de esquadrias , Janelas, Escadas, Corrimões, Kit Portas Prontas, Lambris, Assoalhos, Decks para Piscina, entre outros.');

    // Return view
    return view('front.contact.index');
  }
}
