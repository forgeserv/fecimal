<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\Core\Seo;

class Project extends Model implements HasMedia, AuditableInterface
{
  use AuditableTrait;
  use HasMediaTrait;
  use Sluggable;

  protected $fillable = [
    'slug',
    'title',
    'subtitle',
    'legend',
    'owner',
    'architect',
    'body',
    'year',
    'views',
    'link_extern',
    'downloads',
    'release',
    'position',
    'publish',
    'category_id',
    'user_id'
  ];

  protected $casts = [
    'publish' => 'boolean',
    'release' => 'boolean'
  ];

  /**
   * Auditable Config
   */
  protected $auditInclude = [
    'slug',
    'title',
    'subtitle',
    'legend',
    'owner',
    'architect',
    'link_extern',
    'body',
    'year',
    'views',
    'downloads',
    'release',
    'position',
    'publish',
    'category_id',
    'user_id'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'title'
      ]
    ];
  }

  /**
   * MediaLibrary Config
   */
  public function registerMediaConversions(Media $media = null)
  {
    $this->addMediaConversion('thumb')
          ->fit(Manipulations::FIT_CROP, 150, 150)
          ->quality(85)
          ->performOnCollections('project', 'projects_galleries')
          ->nonQueued();

    $this->addMediaConversion('cover')
          ->quality(85)
          ->performOnCollections('project')
          ->nonQueued()
          ->keepOriginalImageFormat();

     $this->addMediaConversion('crop')
          ->fit(Manipulations::FIT_CROP, 800, 500)
          ->quality(85)
          ->performOnCollections('project')
          ->nonQueued();

    $this->addMediaConversion('header')
          ->quality(85)
          ->performOnCollections('project_header')
          ->nonQueued();

    $this->addMediaConversion('photo')
         ->fit(Manipulations::FIT_CROP, 414, 736)
         ->width(1080)
         ->quality(85)
         ->nonQueued()
         ->performOnCollections('projects_galleries');
  }

  /**
  * Scopes
  */
  public function scopePublished($query)
  {
    $query->where('publish', true);
  }

  public function scopeReleases($query)
  {
    $query->where('release', true);
  }

  public function scopeNotReleases($query)
  {
    $query->where('release', false);
  }

  public function scopePosition($query)
  {
    $query->orderBy('position', 'ASC');
  }

  /**
  * Relations
  */
  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id')->select('id', 'first_name', 'last_name');
  }

  public function seo()
  {
    return $this->morphOne(Seo::class, 'seoable');
  }

  public function category()
  {
    return $this->belongsTo(ProjectCategory::class, 'category_id');
  }

  /**
  * Accesors
  */
  public function getHeaderAttribute()
  {
    $image = $this->getMedia('project_header')->first();

    return isset($image) ? $image->getUrl('header') : null;
  }

  public function getCoverAttribute()
  {
    $image = $this->getMedia('project')->first();

    return isset($image) ? $image->getUrl('cover') : null;
  }

  public function getCropAttribute()
  {
    $image = $this->getMedia('project')->first();

    return isset($image) ? $image->getUrl('crop') : null;
  }

  public function getThumbAttribute()
  {
    $image = $this->getMedia('project')->first();

    return isset($image) ? $image->getUrl('thumb') : null;
  }

  public function getGalleryAttribute()
  {
    $gallery = $this->getMedia('projects_galleries');

    return isset($gallery) ? $gallery : null;
  }

}
