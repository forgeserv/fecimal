<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\Core\Seo;

class ProductCategory extends Model implements AuditableInterface
{
  use AuditableTrait;
  use Sluggable;

  protected $table = 'products_categories';

  protected $fillable = [
    'slug',
    'name',
    'body',
    'position',
    'active',
    'user_id'
  ];

  protected $casts = [
    'active' => 'boolean'
  ];

  /**
  * Auditable Config
  */
  protected $auditInclude = [
    'slug',
    'name',
    'body',
    'position',
    'active',
    'user_id'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  /**
  * Scopes
  */
  public function scopeActived($query)
  {
    return $query->where('active', true);
  }

  public function scopeWhereSlug($query, $slug)
  {
    return $query->where('slug', $slug);
  }

  /**
  * Relations
  */
  public function products()
  {
    return $this->hasMany(Product::class, 'category_id');
  }

  public function agent()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function seo()
  {
    return $this->morphOne(Seo::class, 'seoable');
  }

  /**
  * Mutators
  */
}
