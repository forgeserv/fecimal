<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model implements HasMedia, AuditableInterface
{
  use AuditableTrait;
  use HasMediaTrait;
  use Sluggable;

  protected $fillable = [
    'slug',
    'name',
    'position',
    'publish',
    'category_id',
    'user_id'
  ];

  protected $casts = [
    'publish' => 'boolean'
  ];

  /**
   * Auditable Config
   */
  protected $auditInclude = [
    'slug',
    'name',
    'position',
    'publish',
    'category_id',
    'user_id'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  /**
   * MediaLibrary Config
   */
  public function registerMediaConversions(Media $media = null)
  {
    $this->addMediaConversion('thumb')
          ->fit(Manipulations::FIT_CROP, 250, 250)
          ->quality(85)
          ->performOnCollections('product', 'products_galleries')
          ->nonQueued();

    $this->addMediaConversion('cover')
          ->fit(Manipulations::FIT_CROP, 923, 608)
          ->quality(85)
          ->performOnCollections('product')
          ->nonQueued();

    $this->addMediaConversion('photo')
         ->width(1080)
         ->quality(85)
         ->nonQueued()
         ->performOnCollections('products_galleries');
  }

  /**
  * Scopes
  */
  public function scopePublished($query)
  {
    $query->where('publish', true);
  }

  public function scopePosition($query)
  {
    $query->orderBy('position', 'ASC');
  }

  public function scopeWhereSlug($query, $slug)
  {
    $query->where('slug', $slug);
  }

  /**
  * Relations
  */
  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id')->select('id', 'first_name', 'last_name');
  }

  public function category()
  {
    return $this->belongsTo(ProductCategory::class, 'category_id');
  }

  /**
  * Accesors
  */
  public function getCoverAttribute()
  {
    $image = $this->getMedia('product')->first();

    return isset($image) ? $image->getUrl('cover') : null;
  }

  public function getThumbAttribute()
  {
    $image = $this->getMedia('product')->first();

    return isset($image) ? $image->getUrl('thumb') : null;
  }

  public function getGalleryAttribute()
  {
    $gallery = $this->getMedia('products_galleries');

    return isset($gallery) ? $gallery : null;
  }

}
