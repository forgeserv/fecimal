<?php

namespace App\Models\Release;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;

class Release extends Model implements HasMedia, AuditableInterface
{
  use AuditableTrait;
  use HasMediaTrait;

  protected $fillable = [
    'title',
    'subtitle',
    'legend',
    'button',
    'refer',
    'target',
    'position',
    'active',
    'user_id'
  ];

  protected $casts = [
    'active' => 'boolean'
  ];

  /**
   * Auditable Config
   */
  protected $auditInclude = [
    'title',
    'subtitle',
    'legend',
    'button',
    'refer',
    'target',
    'position',
    'active',
    'user_id'
  ];

  /**
   * MediaLibrary Config
   */
  public function registerMediaConversions(Media $media = null)
  {
    $this->addMediaConversion('mobile')
          ->fit(Manipulations::FIT_CROP, 414, 736)
          ->quality(90)
          ->performOnCollections('release_mobile')
          ->nonQueued()
          ->keepOriginalImageFormat();

    $this->addMediaConversion('desktop')
          ->fit(Manipulations::FIT_CROP, 1920, 1280)
          ->quality(90)
          ->performOnCollections('release_desktop')
          ->nonQueued()
          ->keepOriginalImageFormat();
  }

  /**
  * Scopes
  */
  public function scopeActived($query)
  {
    $query->where('active', true);
  }

  public function scopePosition($query)
  {
    $query->orderBy('position', 'ASC');
  }

  /**
  * Relations
  */
  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id')->select('id', 'first_name', 'last_name');
  }

  /**
  * Accesors
  */
  public function getTargetNameAttribute()
  {
    $target = null;

    switch($this->target):
      case 1:
        $target = 'Abrir na mesma aba';
        break;
      case 2:
        $target = 'Abrir em nova aba';
        break;
    endswitch;

    return $target;
  }

  public function getTargetActionAttribute()
  {
    $target = null;

    switch($this->target):
      case 1:
        $target = '_self';
        break;
      case 2:
        $target = '_blank';
        break;
    endswitch;

    return $target;
  }

  public function getMobileAttribute()
  {
    $image = $this->getMedia('release_mobile')->first();
    return isset($image) ? $image->getUrl('mobile') : null;
  }

  public function getDesktopAttribute()
  {
    $image = $this->getMedia('release_desktop')->first();
    return isset($image) ? $image->getUrl('desktop') : null;
  }
}
