<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;
use Cviebrock\EloquentSluggable\Sluggable;
use Carbon\Carbon;
use App\Models\Core\Seo;

class Post extends Model implements HasMedia, AuditableInterface
{
  use AuditableTrait;
  use HasMediaTrait;
  use Sluggable;

  protected $fillable = [
    'slug',
    'title',
    'subtitle',
    'body',
    'date_publish',
    'views',
    'position',
    'publish',
    'category_id',
    'user_id'
  ];

  protected $casts = [
    'publish' => 'boolean'
  ];

  /**
   * Auditable Config
   */
  protected $auditInclude = [
    'slug',
    'title',
    'subtitle',
    'body',
    'date_publish',
    'views',
    'position',
    'publish',
    'category_id',
    'user_id'
  ];

  protected $dates = [
    'date_publish'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'title'
      ]
    ];
  }

  /**
   * MediaLibrary Config
   */
  public function registerMediaConversions(Media $media = null)
  {
    $this->addMediaConversion('thumb')
          ->fit(Manipulations::FIT_CROP, 414, 736)
          ->quality(85)
          ->performOnCollections('post')
          ->nonQueued();

    $this->addMediaConversion('cover')
          ->fit(Manipulations::FIT_CROP, 923, 608)
          ->quality(85)
          ->performOnCollections('post')
          ->nonQueued();
  }

  /**
  * Scopes
  */
  public function scopePublished($query)
  {
    $query->where('publish', true);
  }

  public function scopePosition($query)
  {
    $query->orderBy('position', 'ASC');
  }

  /**
  * Relations
  */
  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id')->select('id', 'first_name', 'last_name');
  }

  public function category()
  {
    return $this->belongsTo(PostCategory::class, 'category_id');
  }

  public function seo()
  {
    return $this->morphOne(Seo::class, 'seoable');
  }

  /**
  * Mutators
  */
  public function setDatePublishAttribute($input)
  {
    if($input)
      $this->attributes['date_publish'] = Carbon::createFromFormat('d/m/Y', $input)->format('Y-m-d');
  }

  /**
  * Accesors
  */
  public function getCoverAttribute()
  {
    $image = $this->getMedia('post')->first();

    return isset($image) ? $image->getUrl('cover') : null;
  }

  public function getThumbAttribute()
  {
    $image = $this->getMedia('post')->first();

    return isset($image) ? $image->getUrl('thumb') : null;
  }

}
