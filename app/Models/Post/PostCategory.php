<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class PostCategory extends Model implements AuditableInterface
{
  use AuditableTrait;
  use Sluggable;

  protected $table = 'posts_categories';

  protected $fillable = [
    'slug',
    'name',
    'active',
    'user_id'
  ];

  protected $casts = [
    'active' => 'boolean'
  ];

  /**
  * Auditable Config
  */
  protected $auditInclude = [
    'slug',
    'name',
    'active',
    'user_id'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  /**
  * Scopes
  */
  public function scopeActived($query)
  {
    return $query->where('active', true);
  }

  /**
  * Relations
  */
  public function posts()
  {
    return $this->hasMany(Post::class, 'category_id');
  }

  public function agent()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  /**
  * Mutators
  */
}
