<?php

namespace App\Models\Partner;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Partner extends Model implements HasMedia, AuditableInterface
{
  use AuditableTrait;
  use HasMediaTrait;

  protected $fillable = [
    'name',
    'refer',
    'target',
    'position',
    'active',
    'category_id',
    'user_id'
  ];

  protected $casts = [
    'publish' => 'boolean'
  ];

  /**
   * Auditable Config
   */
  protected $auditInclude = [
    'name',
    'refer',
    'target',
    'position',
    'active',
    'category_id',
    'user_id'
  ];

  /**
  * Scopes
  */
  public function scopeActived($query)
  {
    $query->where('active', true);
  }

  public function scopeWhereCategoryId($query, $id)
  {
    $query->whereHas('category', function ($q) use ($id) {
      $q->where('id', $id);
    });
  }

  public function scopePosition($query)
  {
    $query->orderBy('position', 'ASC');
  }

  /**
  * Relations
  */
  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id')->select('id', 'first_name', 'last_name');
  }

  public function category()
  {
    return $this->belongsTo(PartnerCategory::class, 'category_id');
  }

  /**
  * Accesors
  */
  public function getPhotoAttribute()
  {
    $image = $this->getMedia('partner')->first();

    return isset($image) ? $image->getUrl() : null;
  }

}
