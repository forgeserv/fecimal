<?php

namespace App\Models\Message;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use YourAppRocks\EloquentUuid\Traits\HasUuid;

class Message extends Model
{
  use HasUuid;

  protected $fillable = [
    'name',
    'phone',
    'email',
    'subject',
    'body',
    'viewed'
  ];

  protected $casts = [
    'viewed' => 'boolean'
  ];

  /**
  * Scopes
  */
  public function scopeByRoles($query, $roles = ['root'])
  {
    return $query->with('roles')->whereHas('roles', function ($q) use ($roles) {
      $q->whereNotIn('name', '<>', $roles);
    });
  }

  public function scopeNotView($query)
  {
    $query->where('viewed', false);
  }

  public function scopeByAsc($query, $field)
  {
    return $query->orderBy($field, 'ASC');
  }

  /**
  * Relations
  */
  public function users() {
    return $this->belongsToMany(User::class, 'messages_views')
                ->select(['users.id', 'users.first_name'])
                ->orderBy('users.first_name', 'ASC')
                ->whereHas('roles', function($q) {
                  $q->where('name', '!=', 'root');
                });
  }

  /**
   * Mutators
   */
  public function setNameAttribute($input)
  {
    $this->attributes['name'] = titleCaseBR($input);
  }

  public function setEmailAttribute($input)
  {
    $this->attributes['email'] = mb_strtolower($input, 'UTF-8');
  }

  public function setPhoneAttribute($input)
  {
    $this->attributes['phone'] = trim(preg_replace('#[^0-9]#', '', $input));
  }

  public function getProtocolAttribute() {
    return str_pad($this->id, 8, '0', STR_PAD_LEFT);
  }

  public function getSendDateAttribute()
  {
    return $this->created_at->formatLocalized('%d de %B às %H:%M');
  }
}
