<?php

namespace App\Notifications\Messages;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Message extends Notification
{
  use Queueable;

  /**
   * Create a new notification instance.
   *
   * @return void
   */
  public function __construct($contact, $user)
  {
    $this->contact = $contact;
    $this->user = $user;
  }

  /**
   * Get the notification's delivery channels.
   *
   * @param  mixed  $notifiable
   * @return array
   */
  public function via($notifiable)
  {
    return ['mail'];
  }

  /**
   * Get the mail representation of the notification.
   *
   * @param  mixed  $notifiable
   * @return \Illuminate\Notifications\Messages\MailMessage
   */
  public function toMail($notifiable)
  {
    $url = route('admin.messages.index');

    return (new MailMessage)
            ->from(config('panel.email.default'), config('app.name'))
            ->replyTo($this->contact->email, $this->contact->name)
            ->subject($this->contact->subject)
            ->markdown('admin.notifications.message', ['contact' => $this->contact, 'user' => $this->user, 'url' => $url]);
  }
}
