<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('products_categories', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug');
      $table->string('name');
      $table->text('body')->nullable();
      $table->unsignedInteger('position')->nullable();
      $table->boolean('active', 1)->nullable()->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });

    Schema::create('products', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug');
      $table->string('name');
      $table->unsignedInteger('views')->default(0);
      $table->unsignedInteger('position')->nullable();
      $table->boolean('publish', 1)->nullable()->default(0);
      $table->unsignedInteger('category_id')->index();
      $table->foreign('category_id')->references('id')->on('products_categories');
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('products');
    Schema::dropIfExists('products_categories');
  }
}
