<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('messages', function (Blueprint $table) {
      $table->increments('id');
      $table->char('uuid', 36);
      $table->string('name', 100);
      $table->char('phone', 14)->nullable();
      $table->string('email', 100);
      $table->string('subject')->nullable();
      $table->text('body');
      $table->boolean('viewed')->nullable()->default(0);
      $table->timestamps();
    });

    Schema::create('messages_views', function (Blueprint $table) {
      $table->unsignedInteger('message_id')->index();
      $table->foreign('message_id')->references('id')->on('messages')->onDelete('cascade');
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->primary(['message_id', 'user_id']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('messages_views');
    Schema::dropIfExists('messages');
    Schema::dropIfExists('departments_users');
    Schema::dropIfExists('departments');
  }
}
