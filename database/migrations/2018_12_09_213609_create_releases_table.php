<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReleasesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('releases', function (Blueprint $table) {
      $table->increments('id');
      $table->string('title');
      $table->string('subtitle');
      $table->string('legend')->nullable();
      $table->string('button', 100)->nullable();
      $table->string('refer')->nullable();
      $table->smallInteger('target')->unsigned()->nullable();
      $table->unsignedInteger('position')->nullable();
      $table->boolean('active')->nullable()->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('releases');
  }
}
