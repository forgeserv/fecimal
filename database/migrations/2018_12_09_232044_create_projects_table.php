<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('projects_categories', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug');
      $table->string('name');
      $table->boolean('active', 1)->nullable()->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });

    Schema::create('projects', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug');
      $table->string('title');
      $table->string('subtitle');
      $table->string('legend')->nullable();
      $table->string('owner')->nullable();
      $table->string('link_extern')->nullable();
      $table->string('architect')->nullable();
      $table->text('body')->nullable();
      $table->string('year')->nullable();
      $table->unsignedInteger('views')->default(0);
      $table->unsignedInteger('position')->nullable();
      $table->boolean('release', 1)->unsigned()->default();
      $table->boolean('publish', 1)->unsigned()->default();
      $table->unsignedInteger('category_id')->index();
      $table->foreign('category_id')->references('id')->on('projects_categories');
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('projects');
    Schema::dropIfExists('projects_categories');
  }
}
