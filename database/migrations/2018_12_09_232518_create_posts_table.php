<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('posts_categories', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug');
      $table->string('name');
      $table->boolean('active', 1)->nullable()->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });

    Schema::create('posts', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug');
      $table->string('title');
      $table->string('subtitle');
      $table->text('body')->nullable();
      $table->date('date_publish')->nullable();
      $table->unsignedInteger('views')->default(0);
      $table->boolean('publish', 1)->unsigned()->default();
      $table->unsignedInteger('category_id')->index();
      $table->foreign('category_id')->references('id')->on('posts_categories');
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('posts');
    Schema::dropIfExists('posts_categories');
  }
}
