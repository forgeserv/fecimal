<?php

use Illuminate\Database\Seeder;
use App\Models\Auth\Permission;

class PermissionsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // Reset cached roles and permissions
    app()['cache']->forget('spatie.permission.cache');

    // Permissions
    $permissions = [

      // Audit
      ['name' => 'view_audits', 'details' => 'Auditorias [view]'],

      // Dasbhoard
      ['name' => 'view_activities', 'details' => 'Atividades [view]'],

      // Analytics
      ['name' => 'view_analytics', 'details' => 'Acessos [view]'],

      // Messages
      ['name' => 'view_messages', 'details' => 'Sac [view]'],
      ['name' => 'delete_messages', 'details' => 'Sac [delete]'],

      // Reports
      ['name' => 'view_reports', 'details' => 'Relatórios [view]'],

      // Products
      ['name' => 'view_products', 'details' => 'Produtos [view]'],
      ['name' => 'add_products', 'details' => 'Produtos [add]'],
      ['name' => 'edit_products', 'details' => 'Produtos [edit]'],
      ['name' => 'delete_products', 'details' => 'Produtos [delete]'],

      // Products Submodule
      ['name' => 'view_products_submodules', 'details' => 'Produtos Submódulos [view]'],
      ['name' => 'add_products_submodules', 'details' => 'Produtos Submódulos [add]'],
      ['name' => 'edit_products_submodules', 'details' => 'Produtos Submódulos [edit]'],
      ['name' => 'delete_products_submodules', 'details' => 'Produtos Submódulos [delete]'],

      // Posts
      ['name' => 'view_posts', 'details' => 'Blog [view]'],
      ['name' => 'add_posts', 'details' => 'Blog [add]'],
      ['name' => 'edit_posts', 'details' => 'Blog [edit]'],
      ['name' => 'delete_posts', 'details' => 'Blog [delete]'],

      // Posts Submodule
      ['name' => 'view_posts_submodules', 'details' => 'Blog Submódulos [view]'],
      ['name' => 'add_posts_submodules', 'details' => 'Blog Submódulos [add]'],
      ['name' => 'edit_posts_submodules', 'details' => 'Blog Submódulos [edit]'],
      ['name' => 'delete_posts_submodules', 'details' => 'Blog Submódulos [delete]'],

      // Projects
      ['name' => 'view_projects', 'details' => 'Projetos [view]'],
      ['name' => 'add_projects', 'details' => 'Projetos [add]'],
      ['name' => 'edit_projects', 'details' => 'Projetos [edit]'],
      ['name' => 'delete_projects', 'details' => 'Projetos [delete]'],

      // Projects Submodule
      ['name' => 'view_projects_submodules', 'details' => 'Projetos Submódulos [view]'],
      ['name' => 'add_projects_submodules', 'details' => 'Projetos Submódulos [add]'],
      ['name' => 'edit_projects_submodules', 'details' => 'Projetos Submódulos [edit]'],
      ['name' => 'delete_projects_submodules', 'details' => 'Projetos Submódulos [delete]'],

      // Partners
      ['name' => 'view_partners', 'details' => 'Parceiros [view]'],
      ['name' => 'add_partners', 'details' => 'Parceiros [add]'],
      ['name' => 'edit_partners', 'details' => 'Parceiros [edit]'],
      ['name' => 'delete_partners', 'details' => 'Parceiros [delete]'],

      // Partners Submodule
      ['name' => 'view_partners_submodules', 'details' => 'Parceiros Submódulos [view]'],
      ['name' => 'add_partners_submodules', 'details' => 'Parceiros Submódulos [add]'],
      ['name' => 'edit_partners_submodules', 'details' => 'Parceiros Submódulos [edit]'],
      ['name' => 'delete_partners_submodules', 'details' => 'Parceiros Submódulos [delete]'],

      // Releases
      ['name' => 'view_releases', 'details' => 'Destaques [view]'],
      ['name' => 'add_releases', 'details' => 'Destaques [add]'],
      ['name' => 'edit_releases', 'details' => 'Destaques [edit]'],
      ['name' => 'delete_releases', 'details' => 'Destaques [delete]'],

      // Settings
      ['name' => 'view_settings', 'details' => 'Configurações [view]'],
      ['name' => 'add_settings', 'details' => 'Configurações [add]'],
      ['name' => 'edit_settings', 'details' => 'Configurações [edit]'],
      ['name' => 'delete_settings', 'details' => 'Configurações [delete]'],

      // Permissions
      ['name' => 'view_permissions', 'details' => 'Permissões [view]'],
      ['name' => 'add_permissions', 'details' => 'Permissões [add]'],
      ['name' => 'edit_permissions', 'details' => 'Permissões [edit]'],
      ['name' => 'delete_permissions', 'details' => 'Permissões [delete]'],

      // Roles
      ['name' => 'view_roles', 'details' => 'Grupos [view]'],
      ['name' => 'add_roles', 'details' => 'Grupos [add]'],
      ['name' => 'edit_roles', 'details' => 'Grupos [edit]'],
      ['name' => 'delete_roles', 'details' => 'Grupos [delete]'],

      // Users
      ['name' => 'view_users', 'details' => 'Usuários [view]'],
      ['name' => 'add_users', 'details' => 'Usuários [add]'],
      ['name' => 'edit_users', 'details' => 'Usuários [edit]'],
      ['name' => 'delete_users', 'details' => 'Usuários [delete]'],
    ];

    foreach($permissions as $permission):
      Permission::create([
        'name' => $permission['name'],
        'details' => $permission['details'],
        'guard_name' => 'dashboard'
      ]);
    endforeach;
  }
}
