<?php

use Illuminate\Database\Seeder;
use App\Models\Core\Setting;

class SettingsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Setting::create([
      'title' => 'Google Analytics',
      'key' => 'settings.google_analytics',
      'value' => null,
      'active' => false
    ]);

    Setting::create([
      'title' => 'SmartLook',
      'key' => 'settings.smartlook',
      'value' => null,
      'active' => false
    ]);
  }
}
