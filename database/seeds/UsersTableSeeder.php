<?php

use Illuminate\Database\Seeder;
use App\Models\Auth\User;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $joao = User::create([
      'first_name' => 'Joao',
      'last_name' => 'Marcus',
      'email' => 'joaomarcusjesus@gmail.com',
      'phone' => null,
      'password' => bcrypt('senha'),
      'receive_messages' => false,
      'active' => true
    ]);

    $emerson = User::create([
      'first_name' => 'Emerson',
      'last_name' => 'Lemos',
      'email' => 'emersonlemosyb@brzdigital.com.br',
      'phone' => null,
      'password' => bcrypt('senha'),
      'receive_messages' => false,
      'active' => true
    ]);

    $italo = User::create([
      'first_name' => 'Italo',
      'last_name' => 'Fecimal',
      'email' => 'contato@italovinicius.com',
      'phone' => null,
      'password' => bcrypt('senha'),
      'receive_messages' => true,
      'active' => true
    ]);

    $emerson->assignRole('root');
    $italo->assignRole('root');
    $joao->assignRole('root');
  }
}
