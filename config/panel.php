<?php

return [
  'menu' => [
    [
      'name' => 'Destaques',
      'permission' => 'view_releases',
      'route' => 'admin.releases.index'
    ],
    [
      'name' => 'Blog',
      'permission' => 'view_posts',
      'route' => 'admin.posts.index'
    ],
    [
      'name' => 'Projetos',
      'permission' => 'view_projects',
      'route' => 'admin.projects.index'
    ],
    [
      'name' => 'Produtos',
      'permission' => 'view_products',
      'route' => 'admin.products.index'
    ],
    [
      'name' => 'Parceiros',
      'permission' => 'view_partners',
      'route' => 'admin.partners.index'
    ],
    [
      'name' => 'Usuários',
      'permission' => 'view_users',
      'route' => 'admin.users.index'
    ],
/*    [
      'name' => 'Configurações',
      'permission' => 'view_settings',
      'route' => 'admin.settings.index'
    ],*/
  ],
  'audit' => [
    'types' => [
      'Post'                          => 'Post',
      'PostCategory'                  => 'Post Categoria',
      'ProjectCategory'               => 'Projeto Categoria',
      'PartnerCategory'               => 'Parceiro Categoria',
      'Project'                       => 'Projeto',
      'Release'                       => 'Destaque',
      'Product'                       => 'Produto',
      'ProductCategory'               => 'Produto Categoria',
      'Partner'                       => 'Parceiro'
    ],
    'actions' => [
      'created'       => 'Criação',
      'updated'       => 'Alteração',
      'deleted'       => 'Exclusão',
      'restored'      => 'Reativação'
    ],
  ],
  'email' => [
    'default' => 'contato@fecimalartemadeira.com.br'
  ]
];
